FROM nginx:latest

MAINTAINER Patrick Stillhart

VOLUME /var/cache/nginx

# Copy custom nginx config
COPY ./docker/config/nginx.conf /etc/nginx/nginx.conf

# Copy public folder
COPY ./node/public /var/www/public

# Copy ssl keys
COPY ./.certs/wiki/bundle.crt          /etc/nginx/bundle-wiki.crt
COPY ./.certs/wiki/wiki_unlocked.key   /etc/nginx/key-wiki.key

COPY ./.certs/arcs/bundle.crt          /etc/nginx/bundle-arcs.crt
COPY ./.certs/arcs/arcs_unlocked.key   /etc/nginx/key-arcs.key

# Make cert key only available to owner (root)
RUN chmod 600 /etc/nginx/key-wiki.key
RUN chmod 600 /etc/nginx/key-arcs.key

EXPOSE 80 443

ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]

# To build:
# docker build -f docker-nginx.dockerfile --tag danwahlin/nginx ../

# To run:
# docker run -d -p 80:6379 --name nginx danwahlin/nginx
