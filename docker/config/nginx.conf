worker_processes 4;

events { worker_connections 1024; }

http {

        # read more here http://tautt.com/best-nginx-configuration-for-security/
        # and https://gist.github.com/plentz/6737338

        # don't send the nginx version number in error pages and Server header
        server_tokens off;

        # config to don't allow the browser to render the page inside an frame or iframe
        # and avoid clickjacking http://en.wikipedia.org/wiki/Clickjacking
        # if you need to allow [i]frames, you can use SAMEORIGIN or even set an uri with ALLOW-FROM uri
        # https://developer.mozilla.org/en-US/docs/HTTP/X-Frame-Options
        add_header X-Frame-Options SAMEORIGIN;

        #See http://blog.argteam.com/coding/hardening-node-js-for-production-part-2-using-nginx-to-avoid-node-js-load
        proxy_cache_path        /var/cache/nginx levels=1:2 keys_zone=one:8m max_size=3000m inactive=600m;
        proxy_temp_path         /var/tmp;
        include                 mime.types;
        default_type            application/octet-stream;
        sendfile                on;
        keepalive_timeout       65;

        gzip                    on;
        gzip_comp_level         6;
        gzip_vary               on;
        gzip_min_length         1000;
        gzip_proxied            any;
        gzip_types              text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
        gzip_buffers            16 8k;

        upstream node-upstream {
              least_conn;

              server node1:8080 weight=10 max_fails=3 fail_timeout=30s;
              server node2:8080 weight=10 max_fails=3 fail_timeout=30s;
              server node3:8080 weight=10 max_fails=3 fail_timeout=30s;

              keepalive 64;
        }

        upstream arcs-upstream {
              least_conn;

              server arcs:8080 weight=10 max_fails=3 fail_timeout=30s;

              keepalive 64;
        }

        #
        #  CODE WIKI
        #

        server {
            listen   80;
            listen   [::]:80;

            server_name code.wiki www.code.wiki orange;

            return 301 https://$server_name$request_uri;
        }

        server {
              listen 443 ssl default deferred;
              server_name code.wiki www.code.wiki orange;
              root  /var/www/public;

              ssl_certificate     /etc/nginx/bundle-wiki.crt;
              ssl_certificate_key /etc/nginx/key-wiki.key;

              # enable session resumption to improve https performance
              # http://vincent.bernat.im/en/blog/2011-ssl-session-reuse-rfc5077.html
              ssl_session_cache shared:SSL:50m;
              ssl_session_timeout 5m;

              # enables server-side protection from BEAST attacks
              # http://blog.ivanristic.com/2013/09/is-beast-still-a-threat.html
              ssl_prefer_server_ciphers on;
              # disable SSLv3(enabled by default since nginx 0.8.19) since it's less secure then TLS http://en.wikipedia.org/wiki/Secure_Sockets_Layer#SSL_3.0
              ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
              # ciphers chosen for forward secrecy and compatibility
              # http://blog.ivanristic.com/2013/08/configuring-apache-nginx-and-openssl-for-forward-secrecy.html
              ssl_ciphers "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";

              # config to enable HSTS(HTTP Strict Transport Security) https://developer.mozilla.org/en-US/docs/Security/HTTP_Strict_Transport_Security
              # to avoid ssl stripping https://en.wikipedia.org/wiki/SSL_stripping#SSL_stripping
              add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";

              location /style/ {
                access_log off;
                expires 1M;
                add_header Cache-Control "public";
              }

              location /fonts/ {
                access_log off;
                expires 1M;
                add_header Cache-Control "public";
              }

              location /images/ {
                access_log off;
                expires 1M;
                add_header Cache-Control "public";
              }

              location /js/ {
                access_log off;
                expires 1M;
                add_header Cache-Control "public";
              }

              location / {
                  proxy_pass            http://node-upstream;
                  proxy_http_version    1.1;
                  proxy_set_header      Upgrade           $http_upgrade;
                  proxy_set_header      Connection        'upgrade';
                  proxy_set_header      Host              $host;
                  proxy_set_header      X-Real-IP         $remote_addr;
                  proxy_set_header      X-Forwarded-For   $proxy_add_x_forwarded_for;
                  proxy_set_header      X-NginX-Proxy     true;
                  proxy_set_header      X-Forwarded-Proto $scheme;
                  proxy_cache_bypass    $http_upgrade;
              }

              error_page 404 /404;

        }


        #
        #  arcs
        #

        server {
              listen 80;
              listen 443 ssl;
              server_name arcs.io atom.ink;

              ssl_certificate     /etc/nginx/bundle-arcs.crt;
              ssl_certificate_key /etc/nginx/key-arcs.key;

              # enable session resumption to improve https performance
              # http://vincent.bernat.im/en/blog/2011-ssl-session-reuse-rfc5077.html
              ssl_session_cache shared:SSL:50m;
              ssl_session_timeout 5m;

              # enables server-side protection from BEAST attacks
              # http://blog.ivanristic.com/2013/09/is-beast-still-a-threat.html
              ssl_prefer_server_ciphers on;
              # disable SSLv3(enabled by default since nginx 0.8.19) since it's less secure then TLS http://en.wikipedia.org/wiki/Secure_Sockets_Layer#SSL_3.0
              ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
              # ciphers chosen for forward secrecy and compatibility
              # http://blog.ivanristic.com/2013/08/configuring-apache-nginx-and-openssl-for-forward-secrecy.html
              ssl_ciphers "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";

              # config to enable HSTS(HTTP Strict Transport Security) https://developer.mozilla.org/en-US/docs/Security/HTTP_Strict_Transport_Security
              # to avoid ssl stripping https://en.wikipedia.org/wiki/SSL_stripping#SSL_stripping
              add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";

              location / {
                proxy_pass            http://arcs-upstream;
                proxy_http_version    1.1;
                proxy_set_header      Upgrade $http_upgrade;
                proxy_set_header      Connection 'upgrade';
                proxy_set_header      Host $host;
                proxy_set_header      X-Real-IP            $remote_addr;
                proxy_set_header      X-Forwarded-For  $proxy_add_x_forwarded_for;
                proxy_set_header      X-NginX-Proxy    true;
                proxy_cache_bypass    $http_upgrade;
              }
          }


}
