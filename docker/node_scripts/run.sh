#!/bin/bash

if [ -z "$NODE_ENV" ]; then
    export NODE_ENV=development
fi

sleep 10s

forever --spinSleepTime 3000 node/bin/www
