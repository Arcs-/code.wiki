FROM node:carbon

WORKDIR /usr/src/web

COPY node/package*.json ./
RUN npm install --only=production
RUN npm install -g forever

COPY node .
COPY config/ /usr/src/config/

COPY ./docker/node_scripts/run.sh /usr/src/run.sh
RUN chmod +rx /usr/src/run.sh

EXPOSE 8080

ENTRYPOINT ["/usr/src/run.sh"]
