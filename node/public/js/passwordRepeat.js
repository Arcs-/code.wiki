function passwordRepeat() {
	var password = document.getElementById("password")
	var passwordRepeat = document.getElementById("passwordRepeat")
	password.onchange = validatePassword
	passwordRepeat.onchange = validatePassword

	function validatePassword() {
		if (password.value != passwordRepeat.value) passwordRepeat.setCustomValidity("Passwords don't match")
		else passwordRepeat.setCustomValidity('')
	}

}
