(function() {

	// Sooner
	Handlebars.registerHelper('checked', function(checked) {return (checked) ? 'checked' : ''})
	Handlebars.registerHelper('onDefaultIcon', function(icon){ return icon || '/images/profile_default.png'})
	Handlebars.registerHelper('renderType', function(type) {
		switch (type) {
			case 'COMMENT':
				return '<i class="ion-chatbox" title="new comment"></i>'
			case 'NEW_PAGE':
				return '<i class="ion-document" title="new page"></i>'
			case 'EDIT_PAGE':
				return '<i class="ion-android-document" title="page edit"></i>'
		}
	})
	Handlebars.registerHelper('renderTypeToLink', function(type, pageId, changeId) {
		switch (type) {
			case 'COMMENT':
				return '/page/' + pageId + '/discuss#' + changeId
			case 'NEW_PAGE':
				return '/page/' + pageId
			case 'EDIT_PAGE':
				return '/page/' + pageId + '/history/' + changeId
		}
	})

	var navWrapper = document.getElementById('navWrapper')
	var showingUser = false
	if (document.getElementById('navUserContainer') != null) showingUser = true

	function htmlDecode(input) {
		var e = document.createElement('div')
		e.innerHTML = input
		return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue
	}

	window.templates = window.templates || {}
	window.templates.partials = window.templates.partials || {}


  NProgress.configure({ showSpinner: false })

	Sooner.configure({

		hostname: ['127.0.0.1', 'wiki.stillh.art'],

		events: {
			beginLoad: function() {
				NProgress.set(0.4)
			},
			progressLoad: function() {
				NProgress.inc()
			},
			endLoad: function() {
				NProgress.done()
				ga('set', 'page', window.location.href)
			}
		},

		loadTemplate: function(name, callback) {

			var script = document.createElement('script')
			script.src = '/templates/' + name + '.js'
			script.onreadystatechange = callback
			script.onload = callback
			document.head.appendChild(script)

		},

		render: function(data, callback) {

			document.title = htmlDecode(data.title) + ' - wiki'

			if (templates[data.view[0]] != undefined) return callback(templates[data.view[0]][data.view[1]](data))

			function templateLoad() {
				Handlebars.partials = window.templates.partials
				return callback(templates[data.view[0]][data.view[1]](data))
			}

			this.loadTemplate(data.view[0], templateLoad)

		},

		componentRefresh: function(data) {
			if (showingUser != !!data.user) {

				var updateUser = function() {
					var el = document.createElement('div')
					el.innerHTML = templates['partials']['nav'](data)
					navWrapper.lastElementChild.removeChild(navWrapper.lastElementChild.lastElementChild)
					navWrapper.lastElementChild.appendChild(el.lastElementChild.lastElementChild)
					showingUser = !showingUser
				}

				if (window.templates.partials.nav === undefined) this.loadTemplate('main', updateUser)
				else updateUser()

			}
		},

		onError: function(error, href, method, callback) {
			callback([{
				title: 'Ups',
				view: ['main','404'],
				error: error
			}, href])
		}

	})

	// Search

	var searchField = document.getElementById('searchField')
	var suggestions = document.getElementById('suggestions')
	var selected = null

	searchField.addEventListener("keydown", function(e) {
		switch (e.keyCode) {
			case 38:
				if (selected) {
					selected.className = ''
					selected = selected.previousSibling || suggestions.lastChild
				} else selected = suggestions.lastChild
				selected.className = 'active'
				break
			case 40:
				if (selected) {
					selected.className = ''
					selected = selected.nextSibling || suggestions.firstChild
				} else selected = suggestions.firstChild
				selected.className = 'active'
				break
			case 13:
				if (selected) {
					e.preventDefault()
					Sooner.navigate(selected.href)
					suggestions.style.display = "none"
				}
				break
			default:
				selected = null
		}
	})

	searchField.addEventListener("input", function() {

		Sooner.getJSON('get', '/api/search?q=' + encodeURIComponent(searchField.value), 0, function(error, data) {
			if(error) {
				suggestions.style.display = "none"
				console.error(err)
				return
			}


			if (data.length > 0) {
				suggestions.style.display = "block"

				var qRegExp = new RegExp('(^|)(' + searchField.value.split(' ').join('|') + ')(|$)', 'ig')
				var suggestionsContent = ""
				for (let i in data) {
					var text = data[i]._source.title.replace(qRegExp, '$1<b>$2</b>$3')
					suggestionsContent += '<a href="/page/' + data[i]._id + '"><i class="ion-document"></i> &nbsp;' + text + '</a>'
				}

				suggestions.innerHTML = suggestionsContent

			} else {
				suggestions.style.display = "none"
			}

		})

	})

	document.addEventListener("click", function() {
		suggestions.style.display = "none"
	})

	document.addEventListener("keyup", function(e) {
		if (e.keyCode === 70) {
			var tag = (e.target || e.srcElement).tagName
			if (tag != 'INPUT' && tag != 'SELECT' && tag != 'TEXTAREA') searchField.focus()
		}
	})

}())
