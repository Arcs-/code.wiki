this["templates"] = this["templates"] || {};
this["templates"]["manage"] = this["templates"]["manage"] || {};

this["templates"]["manage"]["changes"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<main class=\"container\">\n\n	<div class=\"twelve columns\">\n		<h1>Changes</h1>\n\n		<pre><code id=\"logContainer\" style=\"height: 320px\"></code></pre>\n\n	</div>\n</main>\n\n<script sooner=\"initialize\">\n	var logContainer = document.getElementById('logContainer')\n	var getNiceDateTime = function(date) {\n		date = new Date(date)\n		var m = date.getMonth() + 1\n		var dd = date.getDate()\n		var mm = date.getMinutes()\n		return ((m < 10) ? '0' + m : m) + '.' + ((dd < 10) ? '0' + dd : dd) + ' @ ' + date.getHours() + ':' + ((mm < 10) ? '0' + mm : mm)\n	}\n\n	Sooner.getJSON('get', '/manage/changes/update', null, function(error, logFile) {\n		if (error) return alert(error.statusText)\n\n		var logs = logFile.file,\n			content = ''\n		for (var entry, i = 0; entry = logs[i]; i++) {\n			if (!entry.message) continue\n			content += '[' + getNiceDateTime(entry.timestamp) + '] ' + '[' + entry.level + '] ' + entry.message + '\\n'\n		}\n		logContainer.innerHTML = content\n	})\n</script>\n";
},"useData":true});

this["templates"]["manage"]["review"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "					<tr>\n						<td nowrap>"
    + alias4(((helper = (helper = helpers.version || (depth0 != null ? depth0.version : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"version","hash":{},"data":data}) : helper)))
    + "</td>\n						<td nowrap>"
    + alias4(((helper = (helper = helpers.date || (depth0 != null ? depth0.date : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"date","hash":{},"data":data}) : helper)))
    + "</td>\n						<td nowrap><a href=\"/user/"
    + alias4(((helper = (helper = helpers.user || (depth0 != null ? depth0.user : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"user","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.user || (depth0 != null ? depth0.user : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"user","hash":{},"data":data}) : helper)))
    + "</a></td>\n						<td class=\"full-width\">"
    + alias4(((helper = (helper = helpers.changeTitle || (depth0 != null ? depth0.changeTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"changeTitle","hash":{},"data":data}) : helper)))
    + "</td>\n						<td><a href=\"/manage/review/"
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\" class=\"button small m-0\"><i class=\"ion-aperture\"></i></a></td>\n					</tr>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "						No review tasks\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<main class=\"container\">\n\n	<h1>Review</h1>\n	<div class=\"twelve columns\">\n\n		<table class=\"full-width m-t-20\">\n			<thead>\n				<tr>\n					<th>#</th>\n					<th>Date</th>\n					<th>User</th>\n					<th>Change</th>\n					<th></th>\n				</tr>\n			</thead>\n			<tbody>\n\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.requests : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "\n			</tbody>\n		</table>\n\n	</div>\n\n</main>\n";
},"useData":true});

this["templates"]["manage"]["reviewDetail"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<main class=\"container\">\n\n	<div class=\"twelve columns\">\n		Date: "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.request : depth0)) != null ? stack1.date : stack1), depth0))
    + " by <a href=\"/user/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.request : depth0)) != null ? stack1.user : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.request : depth0)) != null ? stack1.user : stack1), depth0))
    + "</a>\n		<br> Version: <a href=\"/page/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.encodedTitle : stack1), depth0))
    + "\">current "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.version : stack1), depth0))
    + "</a> to "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.request : depth0)) != null ? stack1.version : stack1), depth0))
    + "\n		<br> Comment: <b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.request : depth0)) != null ? stack1.changeTitle : stack1), depth0))
    + "</b>\n		<h4 class=\"m-t-20\">"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.diffTitle : stack1), depth0)) != null ? stack1 : "")
    + "</h4>\n		<pre class=\"reviewContainer\">"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.diffTags : stack1), depth0)) != null ? stack1 : "")
    + "</pre>\n		<b>Content</b>\n		<pre class=\"reviewContainer\">"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.diffContent : stack1), depth0)) != null ? stack1 : "")
    + "</pre>\n		<b>Links</b>\n		<pre class=\"reviewContainer\">"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.diffLinks : stack1), depth0)) != null ? stack1 : "")
    + "</pre>\n\n	</div>\n\n	<form method=\"post\">\n		<button type=\"submit\" name=\"action\" value=\"approve\"><i class=\"ion-checkmark\"></i></button>\n		<button type=\"submit\" name=\"action\" value=\"decline\"><i class=\"ion-close\"></i></button>\n	</form>\n\n</main>\n";
},"useData":true});

this["templates"]["manage"]["sitemap"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<main class=\"container\">\n\n	<div class=\"row\">\n		<h1>Sitemap</h1>\n		<div class=\"twelve columns\">\n			<form method=\"post\" sooner=\"passthrough\">\n				<button type=\"submit\" class=\"button-primary\">Create & Download</button>\n			</form>\n		</div>\n\n	</div>\n\n</main>\n";
},"useData":true});

this["templates"]["manage"]["users"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=helpers.blockHelperMissing, buffer = 
  "						<tr>\r\n							<td><img src=\""
    + alias4(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data}) : helper)))
    + "\" class=\"userIcon\" /></td>\r\n							<td><a href=\"/user/"
    + alias4(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data}) : helper)))
    + "</a></td>\r\n							<td>"
    + alias4(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"email","hash":{},"data":data}) : helper)))
    + "</td>\r\n							<td>"
    + alias4(((helper = (helper = helpers.creation || (depth0 != null ? depth0.creation : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"creation","hash":{},"data":data}) : helper)))
    + "</td>\r\n							<td>"
    + alias4(((helper = (helper = helpers.clearance || (depth0 != null ? depth0.clearance : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"clearance","hash":{},"data":data}) : helper)))
    + "</td>\r\n							<td nowrap><a class=\"m-0 button small\" href=\"/manage/users/action?action=promote&id="
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\" title=\"Promote\"><i class=\"ion-android-arrow-up\"></i></a>\r\n";
  stack1 = ((helper = (helper = helpers.lock || (depth0 != null ? depth0.lock : depth0)) != null ? helper : alias2),(options={"name":"lock","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.lock) { stack1 = alias5.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helper = (helper = helpers.lock || (depth0 != null ? depth0.lock : depth0)) != null ? helper : alias2),(options={"name":"lock","hash":{},"fn":container.noop,"inverse":container.program(4, data, 0),"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.lock) { stack1 = alias5.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "								<a class=\"m-0 button small\" href=\"/manage/users/action?action=delete&id="
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\" title=\"Delete\" onclick=\"return confirm('Delete this user for sure?')\"><i class=\"ion-android-delete\"></i></a></td>\r\n						</tr>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    var helper;

  return "									<a class=\"m-0 button small\" href=\"/manage/users/action?action=lock&id="
    + container.escapeExpression(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"_id","hash":{},"data":data}) : helper)))
    + "\" title=\"Lock\"><i class=\"ion-android-lock\"></i></a>\r\n								";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return "\r\n								<a class=\"m-0 button small\" href=\"/manage/users/action?action=unlock&id="
    + container.escapeExpression(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"_id","hash":{},"data":data}) : helper)))
    + "\" title=\"Unlock\"><i class=\"ion-android-unlock\"></i></a>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<main class=\"container\">\r\n\r\n		<div class=\"twelve columns\">\r\n			<h1>Users</h1>\r\n\r\n			<form>\r\n				<input type=\"text\" placeholder=\"search a user...\" name=\"username\" title=\"Search for a user by username\" />\r\n				<button type=\"submit\" class=\"button small\"><i class=\"ion-android-search\"></i></button>\r\n			</form>\r\n\r\n			<table class=\"full-width\">\r\n				<thead>\r\n					<tr>\r\n						<th></th>\r\n						<th>Username</th>\r\n						<th>Mail</th>\r\n						<th>Creation</th>\r\n						<th>Clearance</th>\r\n						<th>Action</th>\r\n					</tr>\r\n				</thead>\r\n				<tbody>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.users : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				</tbody>\r\n			</table>\r\n\r\n			Page "
    + alias4(((helper = (helper = helpers.pageNumber || (depth0 != null ? depth0.pageNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"pageNumber","hash":{},"data":data}) : helper)))
    + " of "
    + alias4(((helper = (helper = helpers.count || (depth0 != null ? depth0.count : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"count","hash":{},"data":data}) : helper)))
    + "\r\n\r\n		</div>\r\n\r\n	</main>\r\n";
},"useData":true});