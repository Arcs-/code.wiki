this["templates"] = this["templates"] || {};
this["templates"]["main"] = this["templates"]["main"] || {};

this["templates"]["partials"] = this["templates"]["partials"] || {};

this["templates"]["main"]["404"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "	<main class=\"container\">\n\n		<div class=\"twelve columns\">\n\n			<div class=\"twelve columns\">\n\n<pre><code class=\"lang-js\"><span class=\"hljs-comment\">	/*\n	 * not found ( 404 )\n	 * can't find this in our database\n	 */</span>\n	<span class=\"hljs-keyword\">if</span> (!found) {\n	  <span class=\"hljs-keyword\">throw</span> (<span class=\"hljs-string\">' (╯°□°)╯︵ ┻━┻ '</span>)\n	}\n	</code></pre>\n\n		</div>\n	</main>\n";
},"useData":true});

this["templates"]["main"]["error"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "<main class=\"container\">\r\n\r\n		<div class=\"twelve columns\">\r\n			<h1>"
    + alias4(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"message","hash":{},"data":data}) : helper)))
    + "</h1> "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.error : depth0)) != null ? stack1.status : stack1), depth0))
    + "\r\n			<pre><code>"
    + alias4(((helper = (helper = helpers.error || (depth0 != null ? depth0.error : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"error","hash":{},"data":data}) : helper)))
    + "<br><br>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.error : depth0)) != null ? stack1.stack : stack1), depth0))
    + "</code></pre>\r\n\r\n		</div>\r\n	</main>\r\n";
},"useData":true});

this["templates"]["main"]["license"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "	<main class=\"container\">\r\n\r\n		<div class=\"twelve columns\">\r\n			<h1>License</h1>\r\n\r\n			<small>Version: 1.0 from 12.03.2017</small><br /><br />\r\n\r\n			All the text and code (following referred to as content), provided by the\r\n			various authors, are under the MIT license.<br /><br />\r\n\r\nYou are allowed to do anything with the content, as long as you refer to the original page.\r\nIncluding  Commercial Use, Modify, Distribute, sublicense and Private Use.\r\nYou can not hold the authors or wiki.stillh.art liable. The content is provided \"as is\".\r\nWe will neither pay nor aid with a loss you encounter by using it. <br /><br />\r\n\r\nYou shall also include the same license to all places where this code is used and\r\na link to the page of the origin of the contents. This may either be the short or\r\nlong version of a link.<br /><br />\r\n\r\nIf you are participating by publishing or modifying articles you acknowledging and agreeing\r\n to these same turns. Your account name will be added to the history along with the changes you made.\r\n You can not add a custom license to your code.<br /><br />\r\n\r\n\r\n			<pre><code>\r\n			Copyright © 2017 wiki.stillh.art\r\n\r\n Permission is hereby granted, free of charge, to any person obtaining a copy of\r\n this software and associated documentation files (the \"Software\"), to deal in the\r\n Software without restriction, including without limitation the rights to use, copy,\r\n modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,\r\n and to permit persons to whom the Software is furnished to do so, subject to\r\n the following conditions:\r\n\r\n THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,\r\n INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A\r\n PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT\r\n HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION\r\n OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\r\n SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\r\n\r\n</code></pre>\r\n\r\nThe domain the brand, the logo and source code of the page are under strict copy right by Patrick Stillhart\r\n\r\n\r\n		</div>\r\n	</main>\r\n";
},"useData":true});

this["templates"]["main"]["page"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "		<div class=\"disabled\">\n			<div class=\"modal\">\n				<h2>Thanks for your contribution</h2>\n				<p>We'll now review your changes.</p>\n				<button class=\"button-primary\" onclick=\"var p = this.parentNode.parentNode;p.parentNode.removeChild(p)\">close</button>\n			</div>\n		</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "				<a href=\"/search?tags="
    + alias2(alias1(depth0, depth0))
    + "\"><span class=\"tag tag-link\">"
    + alias2(alias1(depth0, depth0))
    + "</span></a>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "\n				"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + " <a href=\""
    + alias4(((helper = (helper = helpers.link || (depth0 != null ? depth0.link : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link","hash":{},"data":data}) : helper)))
    + "\" class=\"right\" rel=\"nofollow\"><i class=\"ion-link\"></i> "
    + alias4(((helper = (helper = helpers.displayLink || (depth0 != null ? depth0.displayLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"displayLink","hash":{},"data":data}) : helper)))
    + "</a>\n				<br>\n\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "					No known\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda, alias3=container.escapeExpression, buffer = 
  "<div id=\"views\">\n	<div id=\"viewContainer\">\n		<a class=\"view large active\">\n			<i class=\"ion-document\"></i>\n		</a>\n		<a title=\"Discuss\" href=\"discuss\" class=\"view\">\n			<i class=\"ion-ios-chatbubble\"></i>\n		</a>\n		<a title=\"Edit\" href=\"edit\" class=\"view\">\n			<i class=\"ion-android-create\"></i>\n		</a>\n	</div>\n	<div class=\"arrow-down\"></div>\n</div>\n\n<main class=\"container hasViews\">\n\n";
  stack1 = ((helper = (helper = helpers.committed || (depth0 != null ? depth0.committed : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"committed","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(alias1,options) : helper));
  if (!helpers.committed) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n	<div class=\"row\">\n		<div class=\"twelve columns\">\n			<h1>"
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.title : stack1), depth0)) != null ? stack1 : "")
    + "</h1>\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.tags : stack1),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\n	</div>\n\n	<div class=\"row m-t-40\">\n\n		<div class=\"twelve columns snippet\">\n\n			"
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.contentRendered : stack1), depth0)) != null ? stack1 : "")
    + "\n\n		</div>\n\n	</div>\n\n	<div class=\"row m-t-100\">\n		<div class=\"seven columns\">\n\n			<h4 class=\"m-t-40\">Reference</h4>\n\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.links : stack1),{"name":"each","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "\n		</div>\n\n		<div class=\"two columns preventCollapse\"></div>\n\n		<div class=\"three columns\">\n			<h4 class=\"m-t-40 preventCollapse\"></h4>\n			<div class=\"nobreak flex\">\n				<a href=\"history\" class=\"grow\"> <i class=\"ion-android-time\"></i> History </a>"
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.lastUpdate : stack1), depth0))
    + "</div>\n			<div class=\"socialContainer\">\n				<input type=\"checkbox\" class=\"checkbox\" id=\"share\" data-link=\"https://wiki.stillh.art/page/"
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "\" data-title=\""
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.title : stack1), depth0)) != null ? stack1 : "")
    + "\" />\n				<label for=\"share\"><i class=\"ion-android-share\"></i> Share </label>\n				<div class=\"social\">\n					<ul>\n						<li id=\"shareTwitter\"><i class=\"ion-social-twitter\"></i></li>\n						<li id=\"shareFacebook\"><i class=\"ion-social-facebook\"></i></li>\n						<li id=\"shareGoogle\"><i class=\"ion-social-googleplus\"></i></li>\n						<li id=\"shareLink\"><i class=\"ion-ios-copy\"></i></li>\n					</ul>\n				</div>\n			</div>\n\n		</div>\n\n	</div>\n\n</main>\n\n<script sooner=\"initialize\">\n	var link = encodeURIComponent(document.getElementById('share').getAttribute('data-link'))\n	var title = 'Learn more about ' + escape(document.getElementById('share').getAttribute('data-title'))\n\n	document.getElementById('shareTwitter').onclick = function() {\n		popupCenter('https://twitter.com/share?url=' + link + '&text=' + title, 'Tweet', '550', '256')\n	}\n\n	document.getElementById('shareFacebook').onclick = function() {\n		popupCenter('https://www.facebook.com/sharer/sharer.php?u=' + link, 'Tweet', '555', '333')\n	}\n\n	document.getElementById('shareGoogle').onclick = function() {\n		popupCenter('https://plus.google.com/share?url=' + link, 'Tweet', '550', '430')\n	}\n\n	var share = document.getElementById('share')\n\n	document.body.addEventListener('click', function(e) {\n\n		var hit = false\n		for (var element, i = 0; element = e.path[i]; i++) {\n			if (element.className && !element.className.indexOf('socialContainer')) {\n				hit = true\n				break\n			}\n		}\n\n		if (!hit) share.checked = false\n\n	})\n\n	// http://stackoverflow.com/a/30810322/3137109\n	document.getElementById('shareLink').onclick = function() {\n		var textArea = document.createElement(\"textarea\")\n		textArea.style.position = 'fixed'\n		textArea.style.top = 0\n		textArea.style.left = 0\n		textArea.style.width = '2em'\n		textArea.style.height = '2em'\n		textArea.style.padding = 0\n		textArea.style.border = 'none'\n		textArea.style.outline = 'none'\n		textArea.style.boxShadow = 'none'\n		textArea.style.background = 'transparent'\n		textArea.value = document.getElementById('share').getAttribute('data-link')\n\n		document.body.appendChild(textArea)\n\n		textArea.select()\n\n		try {\n			document.execCommand('copy')\n		} catch (err) {\n			console.error('Oops, unable to copy')\n		}\n\n		document.body.removeChild(textArea)\n\n	}\n\n\n	// from http://stackoverflow.com/a/16861050/3137109\n	function popupCenter(url, title, w, h) {\n		// Fixes dual-screen position\n		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left\n		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top\n\n		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width\n		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height\n\n		var left = ((width / 2) - (w / 2)) + dualScreenLeft\n		var top = ((height / 2) - (h / 2)) + dualScreenTop\n		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left)\n\n		// Puts focus on the newWindow\n		if (window.focus) newWindow.focus()\n\n	}\n\n</script>\n";
},"useData":true});

this["templates"]["main"]["search"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.pages : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n		<div class=\"eight columns m-t-40\">\n			"
    + ((stack1 = ((helper = (helper = helpers.pageNavigation || (depth0 != null ? depth0.pageNavigation : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"pageNavigation","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n		</div>\n\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.escapeExpression, alias3=container.lambda;

  return "			<div class=\"row\">\n				<a href=\"/page/"
    + alias2(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "/"
    + alias2(alias3(((stack1 = (depth0 != null ? depth0._source : depth0)) != null ? stack1.encodedTitle : stack1), depth0))
    + "\">\n					<div class=\"ten columns searchView\">\n						<div class=\"largeTag\">"
    + alias2(alias3(((stack1 = ((stack1 = (depth0 != null ? depth0._source : depth0)) != null ? stack1.tags : stack1)) != null ? stack1["0"] : stack1), depth0))
    + "</div>\n\n						<div class=\"tagList\">\n							<p class=\"listTitle\">"
    + ((stack1 = alias3(((stack1 = (depth0 != null ? depth0._source : depth0)) != null ? stack1.title : stack1), depth0)) != null ? stack1 : "")
    + "</p>\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0._source : depth0)) != null ? stack1.tags : stack1),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "						</div>\n\n					</div>\n				</a>\n			</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "								<span class=\"tag\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</span>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "		<p class=\"listTitle\">There's nothing on this topic... </p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<main class=\"container\">\n\n	<div class=\"row\">\n		<div class=\"twelve columns\" id=\"filter\">\n\n			<form id=\"editForm\" class=\"m-0 inline\">\n				<input type=\"hidden\" value=\""
    + alias4(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" name=\"q\" />\n\n				<input id=\"tagArray\" name=\"tags\" type=\"hidden\" value=\""
    + alias4(((helper = (helper = helpers.tags || (depth0 != null ? depth0.tags : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tags","hash":{},"data":data}) : helper)))
    + "\" data-key=\""
    + alias4(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" name=\"t\" />\n				<input id=\"transientTagField\" class=\"m-0\" type=\"text\" pattern=\"^([a-zA-Z0-9\\.\\-+]{2,20},?)+$\" title=\"Min: 2 Chars and only [a-z,+,-,.]\" placeholder=\"filter by tags\" />\n				<div id=\"tagContainer\" class=\"inline\" data-drag=\"false\"></div>\n				<input type=\"submit\" id=\"validate\" style=\"position: absolute; left: -9999px\" tabindex=\"-1\" />\n			</form>\n\n			<a href=\"/page/new\" class=\"button m-0 right button-primary\">Publish an Article</a>\n\n		</div>\n	</div>\n\n\n\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.pages : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "\n</main>\n\n<script src=\"/js/tags.js\"></script>\n<script sooner=\"initialize\">\n	initTags()\n</script>\n";
},"useData":true});

this["templates"]["partials"]["nav"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=helpers.blockHelperMissing;

  return "        <div id=\"navUserContainer\">\r\n            <img src=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.icon : stack1), depth0))
    + "\" class=\"userIcon\" onerror=\"this.src='/images/profile_default.png'\"/>\r\n            <span>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.username : stack1), depth0))
    + "</span>\r\n            <ul>\r\n                <li><a href=\"/user\"><i class=\"ion-android-person\"></i>Profile</a></li>\r\n                <li><a href=\"/user/settings\"><i class=\"ion-settings\"></i>Settings</a></li>\r\n"
    + ((stack1 = alias3.call(depth0,alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.moderatorClearance : stack1), depth0),{"name":"user.moderatorClearance","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = alias3.call(depth0,alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.administratorClearance : stack1), depth0),{"name":"user.administratorClearance","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                <li><a href=\"/user/logout\"><i class=\"ion-log-out\"></i>Logout</a></li>\r\n            </ul>\r\n        </div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "                    <li><a href=\"/manage/review\"><i class=\"ion-aperture\"></i>Review</a></li>\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "                    <li><a href=\"/manage/users\"><i class=\"ion-android-people\"></i>Users</a></li>\r\n                    <li><a href=\"/manage/changes\"><i class=\"ion-android-time\"></i>Changes</a></li>\r\n                    <li><a href=\"/manage/sitemap\"><i class=\"ion-android-list\"></i>Sitemap</a></li>\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "        <div id=\"navUserLogin\">\r\n            <a href=\"/user/register\" title=\"Join wiki.stillh.art\">Create account</a> <i class=\"ion-line\"></i> <a href=\"/user/signin\" title=\"Sign into your account\">Sign in</a>\r\n        </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<navigation>\r\n    <a href=\"/\"><img src=\"/images/favicon.png\" title=\"wiki.stillh.art\" id=\"logo\" /></a>\r\n    <div class=\"container middle\">\r\n        <form action=\"/search\" class=\"m-0\">\r\n            <input type=\"search\" id=\"searchField\" value=\""
    + container.escapeExpression(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"search code...\" autocomplete=\"off\" name=\"q\" id=\"searchField\" title=\"Search more code\" />\r\n            <div id=\"suggestions\" style=\"display:none\"></div>\r\n        </form>\r\n    </div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.user : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "</navigation>\r\n";
},"useData":true});