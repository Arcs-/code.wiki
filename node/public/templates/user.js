this["templates"] = this["templates"] || {};
this["templates"]["user"] = this["templates"]["user"] || {};

this["templates"]["user"]["forgot"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "				<p class=\"error\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</p>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "				<p class=\"success\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=helpers.blockHelperMissing, buffer = 
  "<main class=\"container\">\n\n	<div class=\"row\">\n		<div class=\"one-third column preventCollapse\"></div>\n		<div class=\"one-third column keepMinWidth\">\n			<h1 class=\"nobreak\">Reset password</h1>\n			<p>Just enter what you know</p>\n\n";
  stack1 = ((helper = (helper = helpers.error || (depth0 != null ? depth0.error : depth0)) != null ? helper : alias2),(options={"name":"error","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.error) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n";
  stack1 = ((helper = (helper = helpers.success || (depth0 != null ? depth0.success : depth0)) != null ? helper : alias2),(options={"name":"success","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.success) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n			<form method=\"post\">\n				<input type=\"text\" name=\"username_email\" placeholder=\"Username or eMail\" class=\"full-width\" required autofocus/>\n				<input type=\"submit\" value=\"Send\" class=\"right\" />\n			</form>\n\n		</div>\n		<div class=\"one-third column\"></div>\n	</div>\n\n</main>\n";
},"useData":true});

this["templates"]["user"]["profile"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "<i class=\"ion-ios-location\"></i>&nbsp;&nbsp;<span>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</span>\n				<br>";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<i class=\"ion-android-mail\"></i>&nbsp;&nbsp;<a href=\"mailto:"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.email : stack1), depth0))
    + "\" rel=\"nofollow\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.email : stack1), depth0))
    + "</a>\n				<br>";
},"5":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "<i class=\"ion-link\"></i>&nbsp;&nbsp;<a href=\""
    + alias2(alias1(depth0, depth0))
    + "\" rel=\"nofollow\">"
    + alias2(alias1(depth0, depth0))
    + "</a>\n				<br>";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "\n				<a href=\""
    + ((stack1 = (helpers.renderTypeToLink || (depth0 && depth0.renderTypeToLink) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),(depth0 != null ? depth0.pageId : depth0),(depth0 != null ? depth0.changeId : depth0),{"name":"renderTypeToLink","hash":{},"data":data})) != null ? stack1 : "")
    + "\" class=\"actionHistory\">\n								"
    + ((stack1 = (helpers.renderType || (depth0 && depth0.renderType) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"renderType","hash":{},"data":data})) != null ? stack1 : "")
    + " &nbsp; "
    + container.escapeExpression(((helper = (helper = helpers.comment || (depth0 != null ? depth0.comment : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"comment","hash":{},"data":data}) : helper)))
    + "\n						</a>\n\n\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "					"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.username : stack1), depth0))
    + " hasn't done anything yet\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda, alias6=helpers.blockHelperMissing;

  return "<main class=\"container\">\n\n	<div class=\"row\">\n		<div class=\"three columns\">\n			<img src=\""
    + alias4(((helper = (helper = helpers.largeIcon || (depth0 != null ? depth0.largeIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"largeIcon","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.username : stack1), depth0))
    + "\" class=\"full-width\" />\n			<h3 class=\"m-0\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.username : stack1), depth0))
    + "</h3> "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.name : stack1), depth0))
    + "\n\n			<hr/>\n			<i class=\"ion-android-time\"></i>&nbsp;&nbsp;<span>Joined "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.creation : stack1), depth0))
    + "</span>\n			<br>\n			"
    + ((stack1 = alias6.call(depth0,alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.location : stack1), depth0),{"name":"foreignUser.location","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n			"
    + ((stack1 = alias6.call(depth0,alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.showEMail : stack1), depth0),{"name":"foreignUser.showEMail","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n			"
    + ((stack1 = alias6.call(depth0,alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.website : stack1), depth0),{"name":"foreignUser.website","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n\n		</div>\n		<div class=\"five columns preventCollapse\">\n			<b>Pages changed </b> "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.acceptedRequests : stack1), depth0))
    + "\n			<div class=\"snippet\">"
    + ((stack1 = alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.bio : stack1), depth0)) != null ? stack1 : "")
    + "</div>\n		</div>\n		<div class=\"four columns\">\n			<canvas id=\"activityChart\" width=\"600\" height=\"350\" data-from=\""
    + alias4(((helper = (helper = helpers.currentMonth || (depth0 != null ? depth0.currentMonth : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"currentMonth","hash":{},"data":data}) : helper)))
    + "\" data-data=\""
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.timeline : stack1), depth0))
    + "\"></canvas>\n		</div>\n\n		<div class=\"eight columns\">\n			<hr>\n			<h5>Last 7 contributions</h5>\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.foreignUser : depth0)) != null ? stack1.actions : stack1),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "")
    + "\n		</div>\n\n	</div>\n\n</main>\n\n<script src=\"/js/libs/Chart.min.js\"></script>\n<script sooner=\"initialize\">\nvar month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']\n\nfunction rotate(array, index) {\n	for (var i = 0; i < array.length; i++)\n		if (!array[i] || array[i].length == 0) array[i] = undefined\n	return array.splice(index, array.length).concat(array.splice(0, index))\n}\n\nvar activityChart = document.getElementById('activityChart')\nvar currentMonth = activityChart.getAttribute('data-from') * 1 + 1\nvar data = {\n	labels: rotate(month_names_short, currentMonth),\n	datasets: [{\n		label: 'Activity',\n		backgroundColor: 'rgba(30,174,219,0.3)',\n		borderColor: '#2196F3',\n		borderCapStyle: 'butt',\n		pointBorderColor: '#2196F3',\n		pointBackgroundColor: '#fff',\n		pointBorderWidth: 1,\n		pointHoverRadius: 5,\n		pointHoverBackgroundColor: '#2196F3',\n		pointHoverBorderColor: 'rgba(220,220,220,1)',\n		pointHoverBorderWidth: 2,\n		pointRadius: 1,\n		pointHitRadius: 7,\n		data: rotate(activityChart.getAttribute('data-data').split(','), currentMonth),\n	}]\n}\n\nvar options = {\n	scales: {\n		yAxes: [{\n			display: true,\n			ticks: {\n				suggestedMax: 3,\n				suggestedMin: 0\n			}\n		}]\n	}\n}\n\nnew Chart(activityChart, { type: 'line', data: data, options: options })\n</script>\n";
},"useData":true});

this["templates"]["user"]["register"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "				<p class=\"error\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=container.lambda, alias2=container.escapeExpression, buffer = 
  "<main class=\"container\">\n\n	<div class=\"row\">\n		<div class=\"one-third column preventCollapse\"></div>\n		<div class=\"one-third column keepMinWidth\">\n			<h1>Join us!</h1> Already a member? <a href=\"/user/signin\">Sign in</a>\n			<br><br>\n			By registering you are agreeing to the terms under <a href=\"/license\">License</a><br />\n			<br>\n\n";
  stack1 = ((helper = (helper = helpers.error || (depth0 != null ? depth0.error : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"error","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!helpers.error) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n			<form method=\"post\">\n\n				<input type=\"text\" name=\"username\" placeholder=\"Username*\" class=\"full-width\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.proposedUser : depth0)) != null ? stack1.username : stack1), depth0))
    + "\" minLength=\"3\" maxlength=\"20\" required autofocus/>\n				<input type=\"email\" name=\"email\" placeholder=\"Mail*\" class=\"full-width\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.proposedUser : depth0)) != null ? stack1.email : stack1), depth0))
    + "\" />\n\n				<input type=\"password\" name=\"password\" id=\"password\" placeholder=\"Choose a password*\" class=\"full-width m-t-20\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.proposedUser : depth0)) != null ? stack1.password : stack1), depth0))
    + "\" minLength=\"4\" maxlength=\"100\" required/>\n				<input type=\"password\" name=\"passwordRepeat\" id=\"passwordRepeat\" placeholder=\"Repeat that password*\" class=\"full-width\" required/>\n\n				<input type=\"submit\" value=\"Sign up\" class=\"right m-t-20\" />\n\n			</form>\n\n		</div>\n	</div>\n\n</main>\n\n<script src=\"/js/passwordRepeat.js\"></script>\n\n<script sooner=\"initialize\">\n	passwordRepeat()\n</script>\n";
},"useData":true});

this["templates"]["user"]["reset"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "				<p class=\"error\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = 
  "<main class=\"container\">\n\n	<div class=\"row\">\n		<div class=\"one-third column preventCollapse\"></div>\n		<div class=\"one-third column keepMinWidth\">\n			<h1 class=\"nobreak\">Reset password</h1>\n			<p>Now enter your new desired password</p>\n\n";
  stack1 = ((helper = (helper = helpers.error || (depth0 != null ? depth0.error : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"error","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!helpers.error) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n			<form method=\"post\">\n				<input type=\"password\" name=\"password\" id=\"password\" placeholder=\"Choose a password\" class=\"full-width\" value=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.proposedUser : depth0)) != null ? stack1.password : stack1), depth0))
    + "\" minLength=\"4\" maxlength=\"100\" required/>\n				<input type=\"password\" name=\"passwordRepeat\" id=\"passwordRepeat\" placeholder=\"Repeat that password\" class=\"full-width\" required/>\n\n				<input type=\"submit\" value=\"Set\" class=\"right\" />\n			</form>\n\n		</div>\n		<div class=\"one-third column\"></div>\n	</div>\n\n</main>\n\n<script src=\"/js/passwordRepeat.js\" async></script>\n";
},"useData":true});

this["templates"]["user"]["settings"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "                <p class=\"error\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</p>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                <p class=\"success\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {}), alias4=helpers.helperMissing, alias5="function", alias6=helpers.blockHelperMissing, buffer = 
  "<main class=\"container\">\n\n    <div class=\"row\">\n        <div class=\"twelve columns\">\n            <h1>Settings</h1>\n        </div>\n    </div>\n\n    <div class=\"row\">\n        <div class=\"two columns\">\n            <img src=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.realUser : depth0)) != null ? stack1.largeIcon : stack1), depth0))
    + "\" class=\"max-full-width\" />\n        </div>\n        <div class=\"six columns m-t-20\">\n            Profile pictures are served by <a href=\"//gravatar.com/\" target=\"_blank\">Gravatar</a>.\n            <br> Login there to change it.\n        </div>\n    </div>\n\n    <div class=\"row\">\n        <form method=\"post\" class=\"one-half column\">\n            <h3 class=\"m-t-20\"><i class=\"ion-earth\"></i> &nbsp; Profile</h3>\n";
  stack1 = ((helper = (helper = helpers.profileError || (depth0 != null ? depth0.profileError : depth0)) != null ? helper : alias4),(options={"name":"profileError","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias5 ? helper.call(alias3,options) : helper));
  if (!helpers.profileError) { stack1 = alias6.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n";
  stack1 = ((helper = (helper = helpers.profileSuccess || (depth0 != null ? depth0.profileSuccess : depth0)) != null ? helper : alias4),(options={"name":"profileSuccess","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias5 ? helper.call(alias3,options) : helper));
  if (!helpers.profileSuccess) { stack1 = alias6.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n            <input id=\"name\" name=\"name\" type=\"text\" class=\"full-width m-t-20\" placeholder=\"name\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.realUser : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" maxlength=\"40\" />\n            <input id=\"location\" name=\"location\" type=\"text\" class=\"full-width\" placeholder=\"location\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.realUser : depth0)) != null ? stack1.location : stack1), depth0))
    + "\" maxlength=\"40\" />\n            <input id=\"website\" name=\"website\" type=\"text\" class=\"full-width\" placeholder=\"website\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.realUser : depth0)) != null ? stack1.website : stack1), depth0))
    + "\" maxlength=\"70\" />\n\n\n            <label for=\"bio\" class=\"m-t-20\">About you</label>\n            <textarea id=\"bio\" name=\"bio\" class=\"full-width\" maxlength=\"20\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.realUser : depth0)) != null ? stack1.bioRaw : stack1), depth0))
    + "</textarea>\n\n            <input type=\"hidden\" name=\"action\" value=\"profile\" />\n            <input type=\"submit\" class=\"button-primary m-t-20\" value=\"save\" />\n\n        </form>\n\n        <div class=\"one-half column\">\n            <h3 class=\"m-t-20\"><i class=\"ion-locked\"></i> &nbsp; Account</h3>\n";
  stack1 = ((helper = (helper = helpers.accountError || (depth0 != null ? depth0.accountError : depth0)) != null ? helper : alias4),(options={"name":"accountError","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias5 ? helper.call(alias3,options) : helper));
  if (!helpers.accountError) { stack1 = alias6.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n";
  stack1 = ((helper = (helper = helpers.accountSuccess || (depth0 != null ? depth0.accountSuccess : depth0)) != null ? helper : alias4),(options={"name":"accountSuccess","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias5 ? helper.call(alias3,options) : helper));
  if (!helpers.accountSuccess) { stack1 = alias6.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            <form method=\"post\">\n                <label for=\"passwordEmail\">Mail</label>\n                <input type=\"password\" name=\"currentPassword\" id=\"passwordEmail\" placeholder=\"current password\" class=\"full-width\" minLength=\"4\" maxlength=\"100\" required/>\n                <input id=\"email\" name=\"email\" type=\"text\" placeholder=\"mail adress\" class=\"full-width m-0 m-t-20\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.realUser : depth0)) != null ? stack1.email : stack1), depth0))
    + "\" />\n                <input id=\"showemail\" name=\"showemail\" type=\"checkbox\" "
    + alias2((helpers.checked || (depth0 && depth0.checked) || alias4).call(alias3,((stack1 = (depth0 != null ? depth0.realUser : depth0)) != null ? stack1.showEMail : stack1),{"name":"checked","hash":{},"data":data}))
    + ">\n                <label for=\"showemail\" class=\"for-checkbox\">show in profile</label>\n                <br>\n\n                <input type=\"hidden\" name=\"action\" value=\"email\" />\n                <input type=\"submit\" class=\"button-primary m-t-20\" value=\"set mail\" />\n            </form>\n            <form method=\"post\">\n                <label for=\"currentPassword\" class=\"m-t-40\">Password</label>\n\n                <input type=\"password\" name=\"currentPassword\" id=\"currentPassword\" placeholder=\"current password\" class=\"full-width\" minLength=\"4\" maxlength=\"100\" required/>\n\n                <input type=\"password\" name=\"password\" id=\"password\" placeholder=\"new password\" class=\"full-width m-t-20\" minLength=\"4\" maxlength=\"100\" required/>\n                <input type=\"password\" name=\"passwordRepeat\" id=\"passwordRepeat\" placeholder=\"repeat that password\" class=\"full-width\" required/>\n\n                <input type=\"hidden\" name=\"action\" value=\"password\" />\n                <input type=\"submit\" class=\"button-primary m-t-20\" value=\"set password\" />\n            </form>\n        </div>\n\n\n    </div>\n\n\n</main>\n\n\n<style>\n    .CodeMirror,\n    .CodeMirror-scroll {\n        min-height: 150px\n    }\n</style>\n\n<script src=\"/js/libs/simplemde.min.js\"></script>\n<script src=\"/js/passwordRepeat.js\"></script>\n\n<script sooner=\"initialize\">\nlet simplemde = new SimpleMDE({\n	element: document.getElementById('bio'),\n	autoDownloadFontAwesome: false,\n	lineWrapping: false,\n	spellChecker: false,\n	hideIcons: ['fullscreen', 'side-by-side', 'preview'],\n	showIcons: ['code'],\n	shortcuts: {\n		\"toggleFullScreen\": null,\n		\"toggleSideBySide\": null,\n		\"toggleHeadingBigger\": null,\n		\"togglePreview\": null,\n		\"toggleHeadingSmaller\": null\n	},\n	renderingConfig: {\n		singleLineBreaks: true\n	}\n})\n\npasswordRepeat()\n\n</script>\n";
},"useData":true});

this["templates"]["user"]["signin"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "					<p class=\"error\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</p>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "					<p class=\"success\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</p>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=helpers.blockHelperMissing, buffer = 
  "	<main class=\"container\">\r\n\r\n		<div class=\"row\">\r\n			<div class=\"one-third column preventCollapse\"></div>\r\n			<div class=\"one-third column keepMinWidth\">\r\n				<h1>Sign in</h1> Not a member? <a href=\"/user/register\">Create account</a> now\r\n				<br>\r\n				<br>\r\n\r\n";
  stack1 = ((helper = (helper = helpers.error || (depth0 != null ? depth0.error : depth0)) != null ? helper : alias2),(options={"name":"error","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.error) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helper = (helper = helpers.success || (depth0 != null ? depth0.success : depth0)) != null ? helper : alias2),(options={"name":"success","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.success) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n				<form method=\"post\">\r\n					<input type=\"text\" name=\"username\" value=\""
    + container.escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"Username\" class=\"full-width\" required autofocus/>\r\n					<input type=\"password\" name=\"password\" placeholder=\"Password\" class=\"full-width\" required/>\r\n\r\n					<a href=\"/user/forgot\">forgot password?</a>\r\n\r\n					<input type=\"submit\" value=\"Sign in\" class=\"right\" />\r\n				</form>\r\n\r\n			</div>\r\n		</div>\r\n\r\n	</main>\r\n";
},"useData":true});