this["templates"] = this["templates"] || {};
this["templates"]["page"] = this["templates"]["page"] || {};

this["templates"]["partials"] = this["templates"]["partials"] || {};

this["templates"]["page"]["discuss"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "						<input type=\"text\" id=\"newTitle\" placeholder=\"A title...\" name=\"title\" required autocomplete=\"off\" />\n						<textarea id=\"newComment\" cols=\"30\" rows=\"3\" placeholder=\"and a comment...\" name=\"content\" required autocomplete=\"off\"></textarea>\n\n						<div class=\"comment-bottom-row\">\n							<div class=\"comment-actions\">\n								<a class=\"postButton\">Post</a>\n							</div>\n						</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "							Please <a href=\"/user/signin\">Sign in</a> to comment\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "				<div class=\"comment-wrap\" id=\""
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">\n					<a href=\"/user/"
    + alias4(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data}) : helper)))
    + "\">\n						<img src=\""
    + alias4(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data}) : helper)))
    + "\" class=\"avatar\" />\n					</a>\n\n					<div class=\"comment-block\">\n						<div class=\"comment-action-hover\">\n							<h5>"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</h5> "
    + ((stack1 = ((helper = (helper = helpers.content || (depth0 != null ? depth0.content : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"content","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n							<div class=\"comment-bottom-row\">\n								<div class=\"comment-date\">"
    + alias4(((helper = (helper = helpers.date || (depth0 != null ? depth0.date : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"date","hash":{},"data":data}) : helper)))
    + "</div>\n\n								<div class=\"comment-actions\" data-comment=\""
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers.blockHelperMissing.call(depth0,container.lambda(((stack1 = (data && data.root)) && stack1.user), depth0),{"name":"@root.user","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "								</div>\n\n							</div>\n						</div>\n						<!-- sub comments -->\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.replies : depth0),{"name":"each","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "						<!-- end -->\n					</div>\n				</div>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "										<a class=\"flagButton\">Flag</a> <i class=\"ion-line\"></i> <a class=\"replyButton\">Reply</a>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "							<div class=\"comment-wrap\" id=\""
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">\n								<a href=\"/user/"
    + alias4(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data}) : helper)))
    + "\">\n									<img src=\""
    + alias4(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data}) : helper)))
    + "\" class=\"avatar\" />\n								</a>\n\n								<div class=\"comment-block\">\n									<div class=\"comment-action-hover\">\n										<h5>"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</h5> "
    + ((stack1 = ((helper = (helper = helpers.content || (depth0 != null ? depth0.content : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"content","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n										<div class=\"comment-bottom-row\">\n											<div class=\"comment-date\">"
    + alias4(((helper = (helper = helpers.date || (depth0 != null ? depth0.date : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"date","hash":{},"data":data}) : helper)))
    + "</div>\n											<div class=\"comment-actions\" data-comment=\""
    + alias4(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">\n												"
    + ((stack1 = helpers.blockHelperMissing.call(depth0,container.lambda(((stack1 = (data && data.root)) && stack1.user), depth0),{"name":"@root.user","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n											</div>\n										</div>\n									</div>\n\n								</div>\n							</div>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return " <a class=\"flagButton\">Flag</a> ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {}), alias4=helpers.helperMissing, buffer = 
  "<div id=\"views\">\n	<div id=\"viewContainer\">\n		<a title=\"View\" href=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.encodedTitle : stack1), depth0))
    + "\" class=\"view large\">\n			<i class=\"ion-document\"></i>\n		</a>\n		<a class=\"view active\">\n			<i class=\"ion-ios-chatbubble\"></i>\n		</a>\n		<a title=\"Edit\" href=\"edit\" class=\"view\">\n			<i class=\"ion-android-create\"></i>\n		</a>\n	</div>\n	<div class=\"arrow-down\"></div>\n</div>\n\n<main class=\"container hasViews\">\n\n	<div class=\"row\">\n		<div class=\"twelve columns\">\n			<h1>Discussion</h1>\n			<h5>"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.title : stack1), depth0)) != null ? stack1 : "")
    + "</h5>\n		</div>\n	</div>\n\n	<div class=\"row m-t-40\">\n		<div class=\"one column preventCollapse\"></div>\n		<div class=\"ten columns\">\n\n			<div class=\"comment-wrap\">\n				<img src=\""
    + alias2((helpers.onDefaultIcon || (depth0 && depth0.onDefaultIcon) || alias4).call(alias3,((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.icon : stack1),{"name":"onDefaultIcon","hash":{},"data":data}))
    + "\" class=\"avatar\" title=\"you...\" />\n\n				<div class=\"comment-block comment-action-hover\">\n\n";
  stack1 = ((helper = (helper = helpers.user || (depth0 != null ? depth0.user : depth0)) != null ? helper : alias4),(options={"name":"user","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data}),(typeof helper === "function" ? helper.call(alias3,options) : helper));
  if (!helpers.user) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n				</div>\n\n			</div>\n\n\n			<div id=\"replyBox\" style=\"display:none\" class=\"comment-wrap\">\n				<img src=\""
    + alias2((helpers.onDefaultIcon || (depth0 && depth0.onDefaultIcon) || alias4).call(alias3,((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.icon : stack1),{"name":"onDefaultIcon","hash":{},"data":data}))
    + "\" class=\"avatar\" />\n\n				<div class=\"comment-block\">\n					<div class=\"comment-action-hover\">\n						<textarea cols=\"30\" rows=\"3\" placeholder=\"respond...\" name=\"content\" required autocomplete=\"off\"></textarea>\n\n						<div class=\"comment-bottom-row\">\n							<div class=\"comment-actions\">\n								<a class=\"postButton\">Post</a>\n							</div>\n						</div>\n\n					</div>\n				</div>\n			</div>\n\n"
    + ((stack1 = helpers.each.call(alias3,((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.comments : stack1),{"name":"each","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n\n		</div>\n	</div>\n\n</main>\n\n<script sooner=\"initialize\">\n	var replyBox = document.getElementById('replyBox')\n	var currentReply = null\n\n	var replyPost = function(e) {\n\n		var post = {\n			content: currentReply.getElementsByTagName('textarea')[0].value,\n			parent: currentReply.parentNode.parentNode.parentNode.id,\n			action: 'post'\n		}\n\n		Sooner.getJSON('post', window.location.href, post, function(error, posted) {\n			Sooner.navigate( window.location.hash = '#' + posted._id )\n		})\n\n	}\n\n	var reply = function(e) {\n		if (currentReply) currentReply.parentNode.removeChild(currentReply)\n\n		var newBox = replyBox.cloneNode(true)\n		newBox.style.display = ''\n		newBox.id = ''\n		newBox.getElementsByClassName('postButton')[0].onclick = replyPost\n\n		currentReply = e.target.parentNode.parentNode.parentNode.appendChild(newBox)\n\n	}\n\n	var flag = function(e) {\n\n		var post = {\n			id: e.target.parentNode.getAttribute('data-comment'),\n			action: 'flag'\n		}\n\n		Sooner.getJSON('post', window.location.href, post, function(error, status) {\n				setTimeout(function() {\n					alert('Thanks. We\\'ll have a look at it')\n				}, 1)\n		})\n\n	}\n\n	var post = function(e) {\n		var post = {\n			title: document.getElementById('newTitle').value,\n			content: document.getElementById('newComment').value,\n			action: 'post'\n		}\n\n		Sooner.getJSON('post', window.location.href, post, function(error, posted) {\n			Sooner.reload()\n\n			setTimeout(function() {\n				Sooner.navigate('#' + posted._id)\n			}, 200)\n\n		})\n	}\n\n	var replyButtons = document.getElementsByClassName('replyButton')\n	for (var i = 0, replyButton; replyButton = replyButtons[i]; i++) {\n		replyButton.onclick = reply\n	}\n\n	var flagButtons = document.getElementsByClassName('flagButton')\n	for (var i = 0, flagButton; flagButton = flagButtons[i]; i++) {\n		flagButton.onclick = flag\n	}\n\n	var postButtons = document.getElementsByClassName('postButton')\n	for (var i = 0, postButton; postButton = postButtons[i]; i++) {\n		postButton.onclick = post\n	}\n</script>\n";
},"useData":true});

this["templates"]["page"]["edit"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "		<div class=\"disabled\">\n			<div class=\"modal\">\n				<h2>Thanks for contribution</h2>\n				<p>Please give us a few hours to review your commit.<br> With more approval, you won't be limited so much</p>\n			</div>\n		</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "				<button type=\"submit\" title=\"Suggest to remove this post\" name=\"action\" value=\"delete\" onclick=\"return confirm('Delete this article for sure?')\"><i class=\"ion-trash-b\"></i> Remove this Page</a>\n					</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=container.lambda, alias2=helpers.blockHelperMissing, buffer = 
  "<div id=\"views\">\n	<div id=\"viewContainer\">\n		<a title=\"View\" href=\""
    + container.escapeExpression(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.encodedTitle : stack1), depth0))
    + "\" class=\"view large\">\n			<i class=\"ion-document\"></i>\n		</a>\n		<a title=\"Discuss\" href=\"discuss\" class=\"view\">\n			<i class=\"ion-ios-chatbubble\"></i>\n		</a>\n		<a class=\"view active\">\n			<i class=\"ion-android-create\"></i>\n		</a>\n	</div>\n	<div class=\"arrow-down\"></div>\n</div>\n\n<main class=\"container hasViews\">\n\n";
  stack1 = ((helper = (helper = helpers.limitReached || (depth0 != null ? depth0.limitReached : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"limitReached","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!helpers.limitReached) { stack1 = alias2.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n	<div class=\"row\">\n		<div class=\"twelve columns\">\n			<h1>Edit</h1>\n		</div>\n	</div>\n\n	<form method=\"post\" id=\"editForm\">\n\n		<div class=\"twelve columns\">\n\n"
    + ((stack1 = container.invokePartial(partials.pageEdit,depth0,{"name":"pageEdit","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n			<input title=\"What did you change?\" type=\"text\" placeholder=\"What did you change?\" class=\"full-width m-t-80\" name=\"changeTitle\" required/>\n			<br>\n\n			<!-- default action -->\n			<!--<input type=\"hidden\" name=\"action\" value=\"save\" />-->\n			<button type=\"submit \" class=\"button-primary\" title=\"Save the changes you made for review\" name=\"action\" value=\"save\"><i class=\"ion-plus-round\"></i> Save</button>\n"
    + ((stack1 = alias2.call(depth0,alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.moderatorClearance : stack1), depth0),{"name":"user.moderatorClearance","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n		</div>\n\n	</form>\n</main>\n\n<script src=\"/js/tags.js\"></script>\n<script src=\"/js/libs/simplemde.min.js\"></script>\n\n<script sooner=\"initialize\">\n\n		/* tags            */\n		/* =============== */\n		initTags()\n\n		/* simpleMDE       */\n		/* =============== */\n		let simplemde = new SimpleMDE({\n			element: document.getElementById('description'),\n			autoDownloadFontAwesome: false,\n			lineWrapping: false,\n			status: [],\n			spellChecker: true,\n			placeholder: 'share knowledge...',\n			hideIcons: ['fullscreen', 'side-by-side'],\n			showIcons: ['code', 'table'],\n			shortcuts: {\n				\"toggleFullScreen\": null,\n				\"toggleSideBySide\": null,\n				\"toggleHeadingBigger\": null,\n				\"toggleHeadingSmaller\": null\n			},\n			renderingConfig: {\n				singleLineBreaks: true\n			}\n		})\n\n		/* links           */\n		/* =============== */\n		let links = document.getElementById('links')\n		let linkTemplate = document.getElementById('linkTemplate')\n		let addLinkFieldButton = document.getElementById('addLinkField')\n\n		var removeLinkField = function() {\n			let row = this.parentNode.parentNode\n			row.parentNode.removeChild(row)\n		}\n\n		var addLinkField = function() {\n			let clone = linkTemplate.cloneNode(true)\n			clone.classList.remove('none')\n			clone.removeAttribute('id')\n			clone.getElementsByClassName('remove')[0].addEventListener('click', removeLinkField)\n			links.appendChild(clone)\n		}\n\n		addLinkFieldButton.addEventListener('click', addLinkField)\n		if (links.children.length === 1) addLinkField()\n		else for(var i = 1, tmp; tmp = links.children[i++];) tmp.getElementsByClassName('remove')[0].addEventListener('click', removeLinkField)\n\n</script>\n";
},"usePartial":true,"useData":true});

this["templates"]["page"]["history"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "							<tr>\r\n								<td nowrap>"
    + alias4(((helper = (helper = helpers.version || (depth0 != null ? depth0.version : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"version","hash":{},"data":data}) : helper)))
    + "</td>\r\n								<td nowrap>"
    + alias4(((helper = (helper = helpers.date || (depth0 != null ? depth0.date : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"date","hash":{},"data":data}) : helper)))
    + "</td>\r\n								<td nowrap><a href=\"/user/"
    + alias4(((helper = (helper = helpers.user || (depth0 != null ? depth0.user : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"user","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.user || (depth0 != null ? depth0.user : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"user","hash":{},"data":data}) : helper)))
    + "</a></td>\r\n								<td class=\"full-width\"><a href=\"/page/"
    + alias4(container.lambda(((stack1 = (depths[1] != null ? depths[1].page : depths[1])) != null ? stack1._id : stack1), depth0))
    + "/history/"
    + alias4(((helper = (helper = helpers.version || (depth0 != null ? depth0.version : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"version","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.changeTitle || (depth0 != null ? depth0.changeTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"changeTitle","hash":{},"data":data}) : helper)))
    + "</a></td>\r\n							</tr>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "							This post is as fresh as a new born baby\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "	<div id=\"views\">\r\n		<div id=\"viewContainer\">\r\n			<a title=\"View\" href=\"/page/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.encodedTitle : stack1), depth0))
    + "\" class=\"view large\">\r\n				<i class=\"ion-document\"></i>\r\n			</a>\r\n			<a title=\"Discuss\" href=\"/page/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "/discuss\" class=\"view\">\r\n				<i class=\"ion-ios-chatbubble\"></i>\r\n			</a>\r\n			<a title=\"Edit\" href=\"/page/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "/edit\" class=\"view\">\r\n				<i class=\"ion-android-create\"></i>\r\n			</a>\r\n		</div>\r\n		<div class=\"arrow-down\"></div>\r\n	</div>\r\n\r\n	<main class=\"container hasViews\">\r\n\r\n		<div class=\"row\">\r\n			<div class=\"twelve columns\">\r\n				<h1>History</h1>\r\n				<h5>"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.title : stack1), depth0)) != null ? stack1 : "")
    + "</h5>\r\n			</div>\r\n		</div>\r\n\r\n		<div class=\"row mt40\">\r\n\r\n			<div class=\"ten columns\">\r\n\r\n				<table class=\"full-width m-t-20\">\r\n					<thead>\r\n						<tr>\r\n							<th>#</th>\r\n							<th>Date</th>\r\n							<th>User</th>\r\n							<th>Change</th>\r\n						</tr>\r\n					</thead>\r\n					<tbody>\r\n\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.history : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(3, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "\r\n					</tbody>\r\n				</table>\r\n\r\n			</div>\r\n		</div>\r\n\r\n	</main>\r\n";
},"useData":true,"useDepths":true});

this["templates"]["page"]["historyDetail"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "						<span class=\"tag\">"
    + ((stack1 = container.lambda(depth0, depth0)) != null ? stack1 : "")
    + "</span>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "	<div id=\"views\">\r\n		<div id=\"viewContainer\">\r\n			<a title=\"View\" href=\"/page/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.encodedTitle : stack1), depth0))
    + "\" class=\"view large\">\r\n				<i class=\"ion-document\"></i>\r\n			</a>\r\n			<a title=\"Discuss\" href=\"/page/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "/discuss\" class=\"view\">\r\n				<i class=\"ion-ios-chatbubble\"></i>\r\n			</a>\r\n			<a title=\"Edit\" href=\"/page/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "/edit\" class=\"view\">\r\n				<i class=\"ion-android-create\"></i>\r\n			</a>\r\n		</div>\r\n		<div class=\"arrow-down\"></div>\r\n	</div>\r\n\r\n	<main class=\"container hasViews\">\r\n\r\n		<div class=\"twelve columns\">\r\n			On "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.change : depth0)) != null ? stack1.date : stack1), depth0))
    + " by <a href=\"/user/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.change : depth0)) != null ? stack1.user : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.change : depth0)) != null ? stack1.user : stack1), depth0))
    + "</a>\r\n			<br> Change from <a href=\"/page/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1._id : stack1), depth0))
    + "/history/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.oldVersion : stack1), depth0))
    + "\">version "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.oldVersion : stack1), depth0))
    + "</a> to "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.change : depth0)) != null ? stack1.version : stack1), depth0))
    + "\r\n			<br> <b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.change : depth0)) != null ? stack1.changeTitle : stack1), depth0))
    + "</b>\r\n\r\n			<hr>\r\n\r\n			<div class=\"row\">\r\n				<div class=\"twelve columns\">\r\n					<h4>"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.diffTitle : stack1), depth0)) != null ? stack1 : "")
    + " </h4>\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.diffTags : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				</div>\r\n			</div>\r\n\r\n			<pre class=\"reviewContainer\">"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.diffContent : stack1), depth0)) != null ? stack1 : "")
    + "</pre>\r\n\r\n			<h4>Links</h4>\r\n			<pre>"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.diff : depth0)) != null ? stack1.diffLinks : stack1), depth0)) != null ? stack1 : "")
    + "</pre>\r\n\r\n		</div>\r\n\r\n	</main>\r\n";
},"useData":true});

this["templates"]["page"]["new"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<main class=\"container\">\n\n	<form method=\"post\" id=\"editForm\">\n\n		<div class=\"twelve columns snippet\">\n\n"
    + ((stack1 = container.invokePartial(partials.pageEdit,depth0,{"name":"pageEdit","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n			<button class=\"button-primary m-t-40\" title=\"Publish\"><i class=\"ion-plus-round\"></i> Publish</button>\n\n		</div>\n\n	</form>\n\n</main>\n\n<script src=\"/js/tags.js\"></script>\n<script src=\"/js/libs/simplemde.min.js\"></script>\n\n<script sooner=\"initialize\">\n\n	/* tags            */\n	/* =============== */\n	initTags()\n\n	/* simpleMDE       */\n	/* =============== */\n	let simplemde = new SimpleMDE({\n		element: document.getElementById('description'),\n		autoDownloadFontAwesome: false,\n		lineWrapping: false,\n		status: [],\n		spellChecker: true,\n		placeholder: 'share knowledge...',\n		hideIcons: ['fullscreen', 'side-by-side'],\n		showIcons: ['code', 'table'],\n		shortcuts: {\n			\"toggleFullScreen\": null,\n			\"toggleSideBySide\": null,\n			\"toggleHeadingBigger\": null,\n			\"toggleHeadingSmaller\": null\n		},\n		renderingConfig: {\n			singleLineBreaks: true\n		}\n	})\n\n	/* links           */\n	/* =============== */\n	let links = document.getElementById('links')\n	let linkTemplate = document.getElementById('linkTemplate')\n	let addLinkFieldButton = document.getElementById('addLinkField')\n\n	var removeLinkField = function() {\n		let row = this.parentNode.parentNode\n		row.parentNode.removeChild(row)\n	}\n\n	var addLinkField = function() {\n		let clone = linkTemplate.cloneNode(true)\n		clone.classList.remove('none')\n		clone.removeAttribute('id')\n		clone.getElementsByClassName('remove')[0].addEventListener('click', removeLinkField)\n		links.appendChild(clone)\n	}\n\n	addLinkFieldButton.addEventListener('click', addLinkField)\n	if (links.children.length === 1) addLinkField()\n</script>\n";
},"usePartial":true,"useData":true});

this["templates"]["partials"]["pageEdit"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <div class=\"row\">\r\n            <div class=\"one column\">\r\n                <a class=\"button small remove\" title=\"remove this\"><i class=\"ion-close-round\"></i></a>\r\n            </div>\r\n            <div class=\"four columns\">\r\n                <input type=\"text\" name=\"linkTitle\" value=\""
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"Title\" class=\"full-width\" />\r\n            </div>\r\n            <div class=\"seven columns\">\r\n                <input type=\"text\" name=\"linkLink\" value=\""
    + alias4(((helper = (helper = helpers.link || (depth0 != null ? depth0.link : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"Link\" class=\"full-width\" />\r\n            </div>\r\n        </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<input title=\"Title of the post\" type=\"text\" placeholder=\"Title\" class=\"full-width\" name=\"title\" value=\""
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.title : stack1), depth0)) != null ? stack1 : "")
    + "\" autofocus autocomplete=\"off\" required/>\r\n<small>Separate Tags with commas. No whitespace allowed</small>\r\n<br>\r\n<input id=\"tagArray\" name=\"tags\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.tags : stack1), depth0))
    + "\" />\r\n<input id=\"transientTagField\" name=\"transientTagField\" type=\"text\" pattern=\"^([a-zA-Z0-9\\.\\-+]{2,20},?)+$\" title=\"Min: 2 Chars and only [a-z,+,-,.]\" placeholder=\"write a crazy tag\" />\r\n<div id=\"tagContainer\" class=\"inline\" data-drag=\"true\"></div>\r\n<input type=\"submit\" id=\"validate\" style=\"position: absolute; left: -9999px\" tabindex=\"-1\" name=\"action\" value=\"save\" />\r\n\r\n<h2 class=\"m-t-40\">Content</h2>\r\n<textarea id=\"description\" name=\"content\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.content : stack1), depth0))
    + "</textarea>\r\n\r\n<h3 class=\"m-t-40\">Reference</h3>\r\n\r\n<div id=\"links\">\r\n\r\n    <div id=\"linkTemplate\" class=\"row none\">\r\n        <div class=\"one column\">\r\n            <a class=\"button small remove\" title=\"remove this\"><i class=\"ion-close-round\"></i></a>\r\n        </div>\r\n        <div class=\"four columns\">\r\n            <input type=\"text\" name=\"linkTitle\" placeholder=\"Title\" class=\"full-width\" />\r\n        </div>\r\n        <div class=\"seven columns\">\r\n            <input type=\"text\" name=\"linkLink\" placeholder=\"Link\" class=\"full-width\" />\r\n        </div>\r\n    </div>\r\n\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.page : depth0)) != null ? stack1.links : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</div>\r\n\r\n<a id=\"addLinkField\" class=\"button\" title=\"add another link field\"><i class=\"ion-plus-round\"></i> another link</a>\r\n<br>\r\n";
},"useData":true});