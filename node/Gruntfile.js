module.exports = function(grunt) {
	grunt.initConfig({

		sass: {
			dist: {
				options: {
					outputStyle: 'compressed',
					precision: 3
				},
				files: {
					"public/style/main.css": "public-style/scss-light/main.scss",
					"public/style/dark.css": "public-style/scss-dark/main.scss"
				}
			}
		},

		handlebars: {
			compile: {
				options: {
					namespace: function(filename) {
						var names = filename.replace(/modules\/(.*)(\/\w+\.hbs)/, '$1')
						var name = names.split('/')[1]
						if (name.indexOf('.hbs') >= 0) name = 'main'
						return 'templates.' + name
					},
					processName: function(filePath) {
						var name = filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length - 4)
						return name
					},
					compilerOptions: {
						defaultLayout: false,
					}

				},
				files: {
					'public/templates/main.js': ['views/main/*.hbs', 'views/partials/nav.hbs'],
					'public/templates/user.js': ['views/user/*.hbs'],
					'public/templates/page.js': ['views/page/*.hbs', 'views/partials/pageEdit.hbs'],
					'public/templates/manage.js': ['views/manage/*.hbs']
				}
			}
		},

		watch: {
			options: {
				livereload: false,
			},
			styles: {
				files: ['public-style/**/*.scss'], // which files to watch
				tasks: ['sass'],
				options: {
					nospawn: true
				}
			},
			templates: {
				files: ['views/**/*.hbs'], // which files to watch
				tasks: ['handlebars'],
				options: {
					nospawn: true
				}
			}
		}
	})

	grunt.loadNpmTasks('grunt-contrib-sass')
	grunt.loadNpmTasks('grunt-contrib-handlebars')
	grunt.loadNpmTasks('grunt-contrib-watch')
	grunt.loadNpmTasks('grunt-sass')
	grunt.registerTask('default', ['sass', 'handlebars', 'watch'])
}
