#!/bin/bash

gnome-terminal -e "/home/arc/desktop/mongo/bin/mongod" --window-with-profile=Keeping
gnome-terminal -e "/home/arc/desktop/el/bin/elasticsearch" --window-with-profile=Keeping

cd /home/arc/desktop/code.wiki/node

gnome-terminal -e "grunt" --window-with-profile=Keeping

export NODE_ENV=development
export PORT=3000

gnome-terminal -e "npm start | lolcat" --window-with-profile=Keeping
