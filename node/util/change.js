'use strict'

let log = require(getGlobal('getLogger'))('Change')

let regexExtender = require(getUtil('regexExtender'))
let redis = require(getGlobal('getRedis'))('brute')
let mongoose = require('mongoose')
let escape = require('escape-html')
let striptags = require('striptags')
let marked = require('marked')
let hljs = require('highlight.js')
let sessionManager = require(getGlobal('getSession'))
let time = require(getUtil('time'))

let PageChangeHistory = mongoose.model('PageChangeHistory')
let User = mongoose.model('User')

let renderer = new marked.Renderer()
renderer.heading = function(text, level) {
	let escapedText = text.toLowerCase().replace(/[^\w]+/g, '-')
	return '<h2 id="' + escapedText + '"><a name="' + escapedText + '" class="anchor" href="#' + escapedText + '">' +
		'<i class="ion-link"></i></a>' + text + '</h2>'
}

let markedConf = {
	sanitize: true,
	singleLineBreaks: true,
	breaks: true,
	gfm: true,
	renderer,
	highlight: (code, lang) => {
		let hl = hljs.highlightAuto(code, (lang) ? lang.split() : undefined)
		if (hl.language) var usedLang = hl.language
		else var usedLang = (lang) ? lang.split()[0] : ''
		if (usedLang) return `<div class="lang" data-text="${usedLang}"></div>` + hl.value
		else return hl.value
	}
}

function descriptionator(page) {
	let desc = striptags(page.contentRendered.trim())

	if (desc.length > 155) {
		desc = desc.split(' ').slice(0, 155).join(' ')
		desc = desc + ''
	}

	return desc.replace(/\r?\n|\r/g, ' ')

}

module.exports = {

	markedConf,

	jumpLines: function(lines, buffer = 3) {

		let removed = 0
		let last = 0
		let opened = false

		for (let i = 0; i - removed < lines.length; i++) {
			let line = lines[i - removed]

			// check if the chances affects only this line or goes on
			let begin = line.lastIndexOf('<span class="')
			let end = line.indexOf('</span>')

			// chance affetcs this line, maybe is already going on
			if (begin > -1 || end > -1 || opened) {

				// how many elements have to be removed (where are we - what we already removed - buffer)
				let toRemove = Math.max(i - removed - last - buffer, 0)

				if (toRemove > buffer) {
					lines.splice(Math.max(last - buffer, 0), toRemove, '\n<span class="info"> &lt; ' + toRemove + ' identical lines skiped &gt;</span>\n')
					removed += toRemove - 1
				}

				last = i - removed +  buffer

			}

			// set flag if ongo ing
			if (begin > end) opened = true
			else if (begin < end) opened = false

		}

		// remove the lines at the end
		let toRemove = Math.max(lines.length - last, 0)
		if (toRemove > 1) lines.splice(last, toRemove, '\n<span class="info"> &lt; ' + toRemove + ' identical lines skipped &gt;</span>\n')

		return lines

	},

	renderDiff: function(diff) {
		let diffOut = ''
		diff.forEach(function(part) {
			if (part.added) diffOut += '<span class="new">' + escape(part.value).replace(/(?:\r\n|\r|\n)/g, ' ↵\n') + '</span>'
			else if (part.removed) diffOut += '<span class="old">' + escape(part.value).replace(/(?:\r\n|\r|\n)/g, ' ↵\n') + '</span>'
			else diffOut += escape(part.value)
		})

		return diffOut
	},

	doDBChange: function(page, request) {
		/*
		push to history
		*/
		let past = new PageChangeHistory(request)
		past._id = mongoose.Types.ObjectId()
		past.version = page.history.length
		past.isNew = true

		page.history.unshift(past)

		/*
		edit page
		*/
		page.title = request.title
		page.encodedTitle = regexExtender.urlCleaner(request.title)
		page.tags = request.tags
		page.links = request.links
		page.content = request.content
		page.contentRendered = marked(request.content, markedConf)
		page.description = descriptionator(page)
		page.lastUpdate = request.date || time.getNiceDate

		/*
		log action
		*/
		if (request.user != 'anonymous') {

			User
				.findOne({ username: request.user })
				.then(user => {
					if (!user) return log.error(res, 'User findOne', userId, err)

					user.actions.unshift({
						pageId: page._id,
						changeId: past.version,
						comment: past.changeTitle,
						type: 'EDIT_PAGE'

					})

					user.timeline[new Date().getMonth()] += 1
					user.markModified('timeline')

					user.pendingRequests -= 1
					user.acceptedRequests += 1

					user.save()

					sessionManager.reloadUser(user)


				})
				.catch(err => log.error(res, 'User findOne', userId, err))

		} else {
			redis.del(request.ip, function(err, result) {
				if (err) return log.error('Redis del remote address', request.ip, err)

			})
		}


		/*
		finish
		*/
		if (request._id) request.remove()

	},

	descriptionator: descriptionator

}
