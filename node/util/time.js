'use strict'

const month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

module.exports = {

	/**
	  gives date as MMM dd yyyy
	  date param is optional

	  http://stackoverflow.com/a/12409344/3137109
	*/
	getNiceDate: function(date) {
		if (date) var today = new Date(date)
		else var today = new Date()

		let dd = today.getDate()

		return month_names_short[today.getMonth()] + ' ' + ((dd < 10) ? '0' + dd : dd) + ', ' + today.getFullYear()
	},

	getNiceDateMapping: 'MMM DD, YYYY',

	/**
		gives date as MMM dd yyyy @ hh:mm
		date param is optional

	*/
	getNiceDateTime: function(date) {
		if (date) var today = new Date(date)
		else var today = new Date()

		let dd = today.getDate()
		let mm = today.getMinutes()

		return month_names_short[today.getMonth()] + ' ' + ((dd < 10) ? '0' + dd : dd) + ', ' + today.getFullYear() + ' @ ' + today.getHours() + ':' + ((mm < 10) ? '0' + mm : mm)
	}

}
