'use strict'

module.exports = {
	escape: (raw) => raw.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'),
	urlCleaner: (raw) => raw.replace(/ /g, '-').replace(/[^0-9a-zA-Z$\-_.+!*'(),]/g, '')

}
