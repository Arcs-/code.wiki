@echo off

start /min /d "C:\Program Files\MongoDB\Server\3.2\bin" mongod.exe
start /min C:\Users\arc\Desktop\elasticsearch-2.3.2\bin\elasticsearch.bat

cd C:\Users\arc\Desktop\code.wiki\node

start /min cmd /k grunt

timeout /t 3 /nobreak

SET NODE_ENV=development
npm start

@echo on
PAUSE
