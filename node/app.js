'use strict'

/*
	global varriables initalize
*/
global.__rootDir = __dirname

global.getGlobal = (file) => __dirname + '/global/' + file
global.getUtil = (file) => __dirname + '/util/' + file

global.transmit = (req, res, response) => {
	if (req.accepts(['html', 'json']) === 'json') {
		res.set('Cache-Control', 'private, no-cache, no-store, must-revalidate')
		res.set('Expires', '-1')
		res.set('Pragma', 'no-cache')
		res.json(response)
	} else res.render(response.view[0] + '/' + response.view[1], response)
}


let log = require(getGlobal('getLogger'))('app')

/*
	DB initalize
*/
let mongoConnection = require(getGlobal('initMongoDB'))

/*
  Express initalize
*/
let express = require('express')
let expressApp = express()

if (process.env.NODE_ENV !== 'development') expressApp.set('trust proxy', true)

expressApp.use(express.static(__rootDir + '/public'))

require(getGlobal('initHandlebars'))(expressApp)
require(getGlobal('initHelmet'))(expressApp)
require(getGlobal('getSession')).initManager(expressApp)
require(getGlobal('initGeneralExpress'))(expressApp)

process.on('SIGINT', function() {
	log.info('SIGINT: Closing DB connections')
	//mongoConnection.close()
	//redisClient.close()
})

module.exports = expressApp
