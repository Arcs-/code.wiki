'use strict'

global.__rootDir = __dirname
global.getUtil = (file) => __dirname + '/util/' + file
global.getGlobal = (file) => __dirname + '/global/' + file

let mongoConnection = require(getGlobal('initMongoDB'))
let mongoose = require('mongoose')
let sm = require('sitemap')
let configHandler = require(getGlobal('getConfig'))
let fs = require('fs')

let sitemapConfig = configHandler.sitemapConfig
let Page = mongoose.model('Page')

let sitemap = sm.createSitemap({
	hostname: sitemapConfig.hostname,
	cacheTime: sitemapConfig.cacheTime,
	urls: [
		{ url: '/', changefreq: 'daily', priority: 0.8 }
	]
})

Page
	.find()
	.select({
		_id: 1,
		encodedTitle: 1,
		lastUpdate: 1
	})
	.exec()
	.then(pages => {

		pages.forEach(page => {
			sitemap.add({
				url: 'page/' + page._id + '/' + page.encodedTitle,
				changefreq: 'monthly',
				priority: 0.5,
				lastmod: page.lastUpdate,
				lastmodrealtime: true
			})
		})

		fs.writeFileSync("public/sitemap.xml", sitemap.toString())

		mongoConnection.disconnect()

		console.log('%d pages indexed', pages.length)

	})
	.catch(err => {
		console.error(err)
		mongoConnection.disconnect()
	})
