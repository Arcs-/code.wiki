'use strict'

let log = require(getGlobal('getLogger'))('getRedis')

let configHandler = require(getGlobal('getConfig'))


if (process.env.NODE_ENV === 'development') var redis = require("redis-mock")
else var redis = require("redis")

let redisConfig = configHandler.redisConfig
let connections = {}

var options = (redisConfig.password) ? { auth_pass: redisConfig.password } : {}

module.exports = function(name) {
	if (name in connections) return connections[name]
	else {
		//log.info('connecting redis client {' + name + '}')
		if (process.env.NODE_ENV === 'development') var client = redis.createClient()
		else var client = redis.createClient(redisConfig.port, redisConfig.host, options)

		client.on('ready', function() {
			log.info('ready {' + name + '}')
		})

		client.on('warning', function(warning) {
			log.warn(warning)
		})

		client.on('error', function(err) {
			log.error('error', err)
		})

		if (redisConfig.password) {
			client.auth(redisConfig.password);
		}

		return connections[name] = client
	}
}
