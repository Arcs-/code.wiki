'use strict'

let log = require(getGlobal('getLogger'))('initMongoDB')

let fs = require('fs')
let mongoose = require('mongoose')
let cachegoose = require('cachegoose')
let configHandler = require(getGlobal('getConfig'))

let config = configHandler.databaseConfig

log.info('Trying to connect to ' + config.host + '/' + config.database)
var options = {
	keepAlive: 120,
	promiseLibrary: global.Promise,
	autoIndex: config.autoIndex || false
}

if (config.username && config.password) {
	options.user = config.username
	options.pass = config.password
}

mongoose.connect('mongodb://' + config.host + '/' + config.database, options)
mongoose.Promise = global.Promise

let conn = mongoose.connection
conn.on('error', e => log.warn(e))
conn.once('open', _ => log.info('connection open'))

cachegoose(mongoose)

// load all models
fs.readdirSync(__rootDir + '/models').forEach(function(filename) {
	if (~filename.indexOf('.js')) require(__rootDir + '/models/' + filename)
})

module.exports = mongoose
