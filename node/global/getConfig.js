'use strict';

let log = require(getGlobal('getLogger'))('getConfig')

let env = process.env.NODE_ENV

log.info('Node environment: ' + env)
log.info('loading config.' + env + '.json')

module.exports = require('../../config/config.' + env + '.json')
