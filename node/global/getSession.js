'use strict'

const maxAge = 60 * 60 * 24 * 30 * 4 * 1000

let log = require(getGlobal('getLogger'))('getSession')

let session = require('express-session')
let RedisStore = require('connect-redis')(session)
let redisSession = require(getGlobal('getRedis'))('session')
let redisLink = require(getGlobal('getRedis'))('sessionLink')
let mongoose = require('mongoose')

let User = mongoose.model('User')

let store

let options = {
	name: '3kgPotatos_Salt_1cupMayonnaise_2spoonsMustard_HalfAnOnion',
	secret: '10 potatoes aren\'t a scerect but 42 are',
	resave: false,
	saveUninitialized: false,
	maxAge: maxAge,
	cookie: { maxAge }
}

if (process.env.NODE_ENV === 'development') {

	store = new session.MemoryStore()

	options.store = store

} else {

	store = new RedisStore({
		client: redisSession,
		logErrors: log.error
	})

	options.store = store
	options.cookie = {
		secure: true,
		httpOnly: true,
		domain: '.wiki.stillh.art',
		maxAge
	}

}

module.exports = {
	initManager: app => app.use(session(options)),
	setLink: sess => { // TODO add to array (user session should be array)
		redisLink.set(sess.user.username, sess.id, function(err, result) {
			if (err) return log.error('Redis set reset key', query, err)

			redisLink.expire(sess.user.username, maxAge)

		})

	},

	reloadUser: user => {
		redisLink.get(user.username, function(err, sid) {
			if (err) return log.error('Redis get', user.username, err)

			if (sid) {
				store.get(sid, function(err, session) {
					if (err) return log.error('Store get', sid, err)

					session.user = User.syncWithSession(user)
					store.set(sid, session)

				})
			}

		})
	},

	destroy: user => {
		redisLink.get(user.username, function(err, sid) {
			if (err) return log.error('Redis get destroy', user.username, err)

			// othwerwise user is not logged in
			if (sid) store.destroy(sid)

		})
	}

}
