'use strict'

let nodemailer = require('nodemailer')
let hbs = require('nodemailer-express-handlebars')
let configHandler = require(getGlobal('getConfig'))
let config = configHandler.mailConfig

var smtpConfig = {
	host: config.host,
	port: config.port,
	secure: false,
	logger: false,
	auth: {
		user: config.user,
		pass: config.password
	},
	tls: {
		rejectUnauthorized: false
	}
}

let transporter = nodemailer.createTransport(smtpConfig)

let hbsOptions = {
	viewEngine: require('express-handlebars').create({}),
	viewPath: __rootDir + '/views/emails',
	extName: '.hbs'
}
transporter.use('compile', hbs(hbsOptions))


module.exports = {
	send: function(mailOptions) {
		return new Promise(function(resolve, reject) {

			mailOptions.from = config.senderName

			transporter.sendMail(mailOptions, function(err, info) {
				if (err) reject(err)
				else resolve(info)

			})

		})
	},
}
