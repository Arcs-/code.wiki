'use strict'

let helmet = require('helmet')

module.exports = function(app) {

	if (process.env.NODE_ENV === 'development') {
		// nothing for now
	} else {
		app.use(helmet.hsts())

	}

	app.use(helmet.hidePoweredBy())
	app.use(helmet.frameguard())
	app.use(helmet.xssFilter())

}
