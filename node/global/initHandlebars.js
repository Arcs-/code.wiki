'use strict'

let exphbs = require('express-handlebars')
let mongoose = require('mongoose')
let session = require(getGlobal('getSession'))

module.exports = function(app) {

	let handlebars = exphbs({
		defaultLayout: __rootDir + '/views/layout.hbs',
		extname: '.hbs',
		helpers: {
			checked: checked => (checked) ? 'checked' : '',
			onDefaultIcon: icon => icon || '/images/profile_default.png',
			renderType: (type) => {
				switch (type) {
					case 'COMMENT':
						return '<i class="ion-chatbox" title="new comment"></i>'
					case 'NEW_PAGE':
						return '<i class="ion-document" title="new page"></i>'
					case 'EDIT_PAGE':
						return '<i class="ion-android-document" title="page edit"></i>'
				}
			},
			renderTypeToLink: (type, pageId, changeId) => {
				switch (type) {
					case 'COMMENT':
						return '/page/' + pageId + '/discuss#' + changeId
					case 'NEW_PAGE':
						return '/page/' + pageId
					case 'EDIT_PAGE':
						return '/page/' + pageId + '/history/' + changeId
				}
			}
		}
	})

	app.engine('.hbs', handlebars)
	app.set('view engine', '.hbs')

	if (process.env.NODE_ENV === 'development') {
		// nothing for now
	} else {
		app.enable('view cache')

	}


}
