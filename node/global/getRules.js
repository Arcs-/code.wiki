'use strict'

let log = require(getGlobal('getLogger'))('getRules')

let mongoose = require('mongoose')
let scrypt = require('scrypt')

let usernameValidator = /^.{3,20}$/
let emailValidator = require('email-validator')
let passwordValidator = /^.{8,100}$/ // TODO: make it harder again
let usernameBlacklist = ['forgot', 'logout', 'profile', 'user', 'register', 'signin', 'anonymous', 'community', 'reset']

/*
RULES
*/

let validatePassword = proposedUser => new Promise(function(resolve, reject) {
	if (passwordValidator.test(proposedUser.password)) {
		if (proposedUser.password != proposedUser.passwordRepeat) return reject('The passwords don\'t match')
		else return resolve()
	} else return reject('The password must have a minimum of 8 chars (max 100)')
})

let validateUsername = proposedUser => new Promise(function(resolve, reject) {
	if (!usernameValidator.test(proposedUser.username)) return reject('The username shall be at least 3 chars long and a maximum of 20')
	else if (usernameBlacklist.indexOf(proposedUser) > -1) return reject('This username is not allowed')
	else if (emailValidator.validate(proposedUser.username)) return reject('The username may not be an email adress')
	else {
		mongoose.model('User')
			.findOne({ lowerUsername: proposedUser.username && proposedUser.username.toLowerCase() })
			.then(user => {

				if (user === null) return resolve()
				else return reject('This username is taken')

			})
			.catch(err => {
				log.error('User findOne by name', proposedUser.username, err)
				return reject('Sorry, we had an internal error')
			})

	}

})

let validateEMail = proposedUser => new Promise(function(resolve, reject) {

	if (!emailValidator.validate(proposedUser.email)) return reject('This doesn\'t look like a mail adress')
	else {
		mongoose.model('User')
			.findOne({ email: proposedUser.email })
			.then(user => {

				if (user === null) return resolve()
				else return reject('This mail adress is already registered')

			})
			.catch(err => {
				log.error('User findOne by email', proposedUser.email, err)
				return reject('Sorry, we had an internal error')
			})

	}

})


let checkLoginOnLive = (user, password) => new Promise(function(resolve, reject) {
	if (!(user && password && password.trim())) reject('Invalid credentials')

	if (!user.username) return reject('Unkown account')
	else if (user.lock) return reject('Account has been locked')
	else {
		scrypt.verifyKdf(user.password, password)
			.then(result => {
				if (result) return resolve(user)
				else return reject('Wrong password')
			})
			.catch(err => {
				log.error('scrypt verify fail ', err)
				return reject('Internal Error')
			})
	}

})


let checkLogin = (username, password) => new Promise(function(resolve, reject) {
	if (!(username && username.trim() && password && password.trim())) reject('Invalid credentials')
	mongoose.model('User')
		.findOne({ lowerUsername: username.toLowerCase() })
		.then(user => checkLoginOnLive(user, password))
		.then(user => resolve(user))
		.catch(err => reject(err))

})

module.exports = {
	validatePassword,
	validateUsername,
	validateEMail,
	checkLoginOnLive,
	checkLogin

}
