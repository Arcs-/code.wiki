'use strict'

let log = require(getGlobal('getLogger'))('getBrute')


let ExpressBrute = require('express-brute')
let RedisStore = require('express-brute-redis')
let redisSession = require(getGlobal('getRedis'))('brute')

if (process.env.NODE_ENV === 'development') var store = new ExpressBrute.MemoryStore()
else var store = new RedisStore({
	client: redisSession,
	prefix: 'brute-'
})

let failCallback = function(req, res, next, nextValidRequestDate) {
	req.tooManyRequest = true
	req.nextValidRequestDate = nextValidRequestDate

	next()
}

let handleStoreError = function(error) {
	log.error('handle store error', error.message, error.parent) // log this error so we can figure out what went wrong

}

// Start slowing requests after 5 failed attempts to do something for the same user
let userBruteforce = new ExpressBrute(store, {
	freeRetries: 5,
	minWait: 5 * 60 * 1000, // 5 minutes
	maxWait: 60 * 60 * 1000, // 1 hour,
	failCallback: failCallback,
	handleStoreError: handleStoreError
})

// No more than 1000 login attempts per day per IP
let globalBruteforce = new ExpressBrute(store, {
	freeRetries: 1000,
	attachResetToRequest: false,
	refreshTimeoutOnRequest: false,
	minWait: 25 * 60 * 60 * 1000, // 1 day 1 hour (should never reach this wait time)
	maxWait: 25 * 60 * 60 * 1000, // 1 day 1 hour (should never reach this wait time)
	lifetime: 24 * 60 * 60, // 1 day (seconds not milliseconds)
	failCallback: failCallback,
	handleStoreError: handleStoreError
})

module.exports = {

	userBruteforce: userBruteforce.getMiddleware({ key: (req, res, next) => next(req.body.username) }),
	globalBruteforce: globalBruteforce.prevent

}
