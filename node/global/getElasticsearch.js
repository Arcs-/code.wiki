'use strict'

let log = require(getGlobal('getLogger'))('getElasticsearch')

let elasticsearch = require("elasticsearch")
let configHandler = require(getGlobal('getConfig'))

let config = configHandler.elasticsearchConfig

let client = new elasticsearch.Client({
	host: config.host + ':9200',
	deadTimeout: 5000,
	level: 'error'
})

client.ping({
	requestTimeout: 10000
}, function(error) {
	if (error) log.error('elasticsearch cluster is down!')
	else log.info('all is well')
})

module.exports = client
