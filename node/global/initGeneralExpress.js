'use strict'

let winston = require('winston')
let expressWinston = require('express-winston')
let fs = require('fs')
let compress = require('compression')
let path = require('path')
let favicon = require('serve-favicon')
let cookieParser = require('cookie-parser')
let bodyParser = require('body-parser')
let time = require(getUtil('time'))
let marked = require('marked')
let hljs = require('highlight.js')

const content = marked(
	[
		'```js',
		'/*',
		' * not found',
		' * can\'t find it in our database',
		' */',
		'if (!found) {',
		'  throw (\' (╯°□°)╯︵ ┻━┻ \')',
		'}',
		'```'
	].join('\n'), {
		sanitize: true,
		singleLineBreaks: true,
		highlight: (code, lang) => hljs.highlightAuto(code, (lang) ? lang.split() : undefined).value
	})

module.exports = function(app) {

	// express-winston logger makes sense BEFORE the router.
	app.use(expressWinston.logger({
		transports: [
			new winston.transports.Console({
				//colorize: true,
				formatter: function(options) {
					// should work behind a proxy
					let ip = options.meta.req.headers['x-forwarded-for'] || options.meta.req.ip || 'no-ip'

					return options.level + ': [Express] ' + time.getNiceDateTime() + ' ' +
						ip + ' ' + options.meta.res.statusCode + ' ' + options.meta.responseTime + ' ' +
						(undefined !== options.message ? options.message : '')
				}



			})
		]
	}))


	// done by nginx
	//app.use(compress())

	app.use(favicon(path.join(__rootDir, 'public', 'images/favicon.png')))

	app.use(bodyParser.json())
	app.use(bodyParser.urlencoded({ extended: false }))


	app.use(cookieParser())

	// Set routes
	// app.use('/', require(__rootDir + '/routes/index'))
	app.use('/', require(__rootDir + '/routes/search'))

	fs.readdirSync(__rootDir + '/routes').forEach(function(filename) {
		if (~filename.indexOf('.js')) app.use('/' + filename.slice(0, -3), require(__rootDir + '/routes/' + filename))
	})

	// catch 404 and forward to error handler
	app.use(function(req, res, next) {
		res.status(404)
		transmit(req, res, {
			titel: '404',
			view: ['main', '404'],
			user: req.session.user,
			content
		})
	})

	// error handlers

	// development error handler
	// will print stacktrace
	if (process.env.NODE_ENV === 'development') {
		app.use(expressWinston.errorLogger({
			transports: [
				new winston.transports.Console({
					json: true
				})
			]
		}))

		app.use(function(err, req, res, next) {
			res.status(err.status || 500)
			transmit(req, res, {
				titel: 'Error',
				view: ['main', 'error'],
				user: req.session.user,
				message: err.message,
				error: err
			})
		})

	} else {
		app.use(expressWinston.errorLogger({
			transports: [
				new winston.transports.Console({
					json: true
				})
			]
		}))

		app.use(function(err, req, res, next) {
			res.status(err.status || 500)
			transmit(req, res, {
				titel: 'Error',
				view: ['main', 'error'],
				user: req.session.user,
				message: err.message
			})
		})
	}
}
