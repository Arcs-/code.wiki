'use strict'

let winston = require('winston')
let _ = require('underscore')

//winston.handleExceptions(new winston.transports.File({ filename: __rootDir + '/exceptions.log' }))

var logger = new(winston.Logger)({
	transports: [
		new(winston.transports.Console)(),
		new(winston.transports.File)({ filename: 'complete.log' })
	]
})

class Logger {
	constructor(name) {
		// would be ready for containers
		this.name = '[' + name + ']'
	}
	error(res, message, detail, err) {
		if (typeof res === 'string') logger.error(this.name, res, '{', message, '}')
		else {

			if (err != null) logger.error(this.name, message, '{', detail, '}', '{', err, '}')
			else logger.error(this.name, message, '{', detail, '}')

			res.redirect('/error')

		}
	}
	warn(...args) {
		logger.warn(this.name, ...args)
	}
	info(...args) {
		logger.info(this.name, ...args)
	}
	getLogger() {
		return logger
	}
	read(options) {
		let defaults = {
			from: new Date - 24 * 60 * 60 * 1000,
			until: new Date,
			limit: 10,
			start: 0,
			order: 'desc',
			fields: ['timestamp', 'level', 'message']
		}
		_.extend(defaults, options)

		return new Promise(function(resolve, reject) {

			logger.query(defaults, function(err, results) {
				resolve(results)
			})

		})

	}
}

module.exports = (name) => new Logger(name)
