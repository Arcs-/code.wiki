'use strict'

let log = require(getGlobal('getLogger'))('Page Model')

let mongoose = require('mongoose')
let shortId = require('shortid')
let elasticsearch = require(getGlobal('getElasticsearch'))
let mexp = require('mongoose-elasticsearch-xp')
let Schema = mongoose.Schema
let time = require(getUtil('time'))

let CommentSchema = new Schema({
	title: String,
	content: String,
	date: {
		type: String,
		default: time.getNiceDateTime
	},
	username: String,
	icon: String,
	replies: [this]
})

mongoose.model('Comment', CommentSchema)

let LinkSchema = new Schema({
	title: String,
	link: String,
	displayLink: String
})

let PageChangeSchema = new Schema({
	pageId: String,
	version: Number,
	date: {
		type: String,
		default: time.getNiceDate
	},
	user: String,
	ip: String,
	changeTitle: String,

	title: String,
	tags: [String],
	content: String,
	links: {
		type: [LinkSchema]
	}

})

mongoose.model('PageChangeRequest', PageChangeSchema)
mongoose.model('PageChangeHistory', PageChangeSchema)

let PageSchema = new Schema({
	_id: {
		type: String,
		default: shortId.generate,
		es_indexed: true
	},

	version: Number,
	lastUpdate: {
		type: String,
		default: time.getNiceDate,
		es_indexed: true,
		es_type: 'date',
		es_value: val => new Date(val.replace(',',''))
	},

	title: {
		type: String,
		es_indexed: true
	},
	encodedTitle: {
		type: String,
		es_indexed: true
	},

	tags: {
		type: [String],
		es_indexed: true
	},

	description: String,

	content: String,
	contentRendered: String,

	links: {
		type: [LinkSchema],
		es_indexed: false
	},

	comments: {
		type: [CommentSchema],
		es_indexed: false
	},

	history: {
		type: [PageChangeSchema],
		es_indexed: false
	}

})

PageSchema.plugin(mexp, { client: elasticsearch })
let Page = mongoose.model('Page', PageSchema)

/*
 initialize
 */
Page.on('es-bulk-sent', _ => log.info('buffer sent'))
Page.on('es-bulk-error', e => log.warn(e))

Page
  .esSynchronize()
  .then(_ => log.info('synchronized.'))
	.catch(e => log.warn('sync error'))
