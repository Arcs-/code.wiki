'use strict'

let log = require(getGlobal('getLogger'))('User Model')

let mongoose = require('mongoose')
let Schema = mongoose.Schema
let time = require(getUtil('time'))
let gravatar = require('gravatar')

/*
	Action
*/
let actionSchema = new Schema({
	pageId: String,
	changeId: String,
	comment: String,
	type: {
		type: String,
		enum: ['COMMENT', 'NEW_PAGE', 'EDIT_PAGE']
	}

})

// if changed, change rendering in Handlebars

mongoose.model('Action', actionSchema)

/*
	User
*/
// will be field from schema
let sessionFieldsCache = []

let userSchema = new Schema({
	username: {
		type: String,
		trim: true,
		loadToSession: true
	},
	lowerUsername: {
		type: String,
		trim: true,
		index: true,
		text: true,
		unique: true,
	},
	name: {
		type: String,
		trim: true,
		loadToSession: true
	},
	email: {
		type: String,
		index: true,
		trim: true
	},
	showEMail: Boolean,
	icon: {
		type: String,
		loadToSession: true
	},
	password: Buffer,
	creation: {
		type: String,
		default: time.getNiceDate
	},
	bio: {
		type: String,
		trim: true
	},
	bioRaw: {
		type: String,
		trim: true
	},
	location: {
		type: String,
		trim: true
	},
	website: {
		type: String,
		trim: true
	},
	lock: Boolean,
	clearance: {
		type: Number,
		default: 1
	},
	pendingRequests: {
		type: Number,
		default: 0,
		loadToSession: true
	},
	acceptedRequests: {
		type: Number,
		default: 0,
		loadToSession: true
	},
	deniedRequests: {
		type: Number,
		default: 0,
		loadToSession: true
	},
	actions: [actionSchema],
	timeline: [Number]
})

/*
	Methods
*/
userSchema.methods.hasClearance = function(role) {
	switch (role) {
		case 'administrator':
			return this.clearance >= 3
		case 'moderator':
			return this.clearance >= 2
		case 'user':
			return this.clearance >= 1
		default:
			log.error("Unkown role", role)
	}
}

userSchema.methods.promote = function() {
	this.clearance += 1
	if (this.clearance > 3) this.clearance = 1
}

/*
	Statics
*/

userSchema.statics.shouldBlock = function(user) {
	if(this.clearance === 3) return false

	if (user.acceptedRequests < 10) return user.pendingRequests >= 3
	else if (user.acceptedRequests < 30) return user.pendingRequests >= 8
	else if (user.acceptedRequests < 60) return user.pendingRequests >= 15
	else {
		log.error("Consider lifting shouldBlock limits..", user.username, user.pendingRequests)
		return user.pendingRequests >= 20
	}

}

userSchema.statics.syncWithDB = function(sessionUser, fields) {
	mongoose.model('User').findOne({
		_id: sessionUser._id
	}, function(err, dbUser) {
		if (err || !dbUser) return log.error('User findOne', sessionUser.username, sessionUser._id, err)

		for (let field of fields) {
			if (sessionFieldsCache.indexOf(field) === -1) {
				log.error('Field {' + field + '} not allowed in session user. it can\'t be saved')
				continue
			}

			dbUser[field] = sessionUser[field]

		}

		dbUser.save()

	})

}

userSchema.statics.syncWithSession = function(dbUser) {
	let user = {}

	// Virtuals
	user['_id'] = dbUser._id
	if(dbUser.hasClearance('moderator')) user['moderatorClearance'] = true
	if(dbUser.hasClearance('administrator')) user['administratorClearance'] = true

	for (let field of sessionFieldsCache) {
		user[field] = dbUser[field]
	}

	return user

}

/*
	Mongoose functions
*/
userSchema.pre('save', function(next) {

	if (this.isNew) {
		this.timeline[11] = undefined
		this.timeline[new Date().getMonth()] = 0

		this.icon = gravatar.url((this.email || ''), { s: '30', d: 'https://wiki.stillh.art/images/profile_default.png' })
		this.lowerUsername = this.username.toLowerCase()
	}

	next()
})

userSchema.set('toJSON', { virtuals: true })
mongoose.model('User', userSchema)

for (let pathName in userSchema.paths) {
	if (userSchema.paths[pathName].options.loadToSession) {
		sessionFieldsCache.push(pathName)
	}
}
