'use strict'

let router = require('express').Router()

router.get('/', function(req, res, next) {
	let sess = req.session

	transmit(req, res, {
		title: 'Ups',
		view: ['main', 'error'],

		user: sess.user,
		message: 'Damn you! you broke it',
		error: sess.error || '// :('
	})

})

module.exports = router
