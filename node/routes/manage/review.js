let log = require(getGlobal('getLogger'))('Review')

let redis = require(getGlobal('getRedis'))('brute')
let mongoose = require('mongoose')
let cachegoose = require('cachegoose')
let sessionManager = require(getGlobal('getSession'))
let JsDiff = require('diff') // https://www.npmjs.com/package/diff
let change = require(getUtil('change'))
let escape = require('escape-html')
let url = require('url')

let Page = mongoose.model('Page')
let PageChangeRequest = mongoose.model('PageChangeRequest')
let User = mongoose.model('User')

router.get('/review', function(req, res, next) {
	let sess = req.session

	PageChangeRequest
		.find({})
		.then(requests => {

			transmit(req, res, {
				title: 'Review',
				view: ['manage', 'review'],
				user: sess.user,
				requests,
			})

		})
		.catch(err => log.error(res, 'PageChangeRequest findAll', err))

})

router.get('/review/:id', function(req, res, next) {
	let id = req.params.id
	let sess = req.session

	PageChangeRequest
		.findOne({ _id: id })
		.then(request => {

			Page
				.findOne({ _id: request.pageId })
				.select({
					title: 1,
					encodedTitle: 1,
					tags: 1,
					content: 1,
					links: 1
				})
				.exec()
				.then(page => {

					let diffTitle = change.renderDiff(JsDiff.diffWords(page.title, request.title))
					let diffTags = change.renderDiff(JsDiff.diffWords(page.tags.join(' | '), request.tags.join(' | ')))

					/*
					Build up chance on content
					*/
					let diffContent = change.renderDiff(JsDiff.diffWords(page.content, request.content))
					let diffContentArray = diffContent.split('\n')
					diffContentArray = change.jumpLines(diffContentArray, 5)

					diffContent = diffContentArray.join('\n') + '\n'

					/*
						Links
					*/
					let diffLinksArray = []
					let max = Math.max(page.links.length, request.links.length)
					for (let i = 0; i < max; i++) {
						if (page.links && page.links[i]) var stringOld = page.links[i].title + ' <i class="ion-android-arrow-forward"></i> ' + page.links[i].link
						else var stringOld = ''

						if (request.links && request.links[i]) var stringnew = request.links[i].title + ' <i class="ion-android-arrow-forward"></i> ' + request.links[i].link
						else var stringnew = ''

						diffLinksArray.push(change.renderDiff(JsDiff.diffWords(stringOld, stringnew)))
					}

					diffLinksArray = change.jumpLines(diffLinksArray, 0)

					let diffLinks = diffLinksArray.join('\n') + '\n'

					transmit(req, res, {
						title: 'Review Detail',
						view: ['manage', 'reviewDetail'],
						user: sess.user,
						request,
						page,
						diff: {
							diffTitle,
							diffTags,
							diffContent,
							diffLinks
						}
					})

				})
				.catch(err => log.error(res, 'Page findOne', request.pageId, err))

		})
		.catch(err => log.error(res, 'PageChangeRequest findOne', id, err))

})

router.post('/review/:id', function(req, res, next) {
	let id = req.params.id
	let action = req.body.action
	let sess = req.session

	switch (req.body.action) {
		case 'approve':
			approve(req, res)
			break
		case 'decline':
			decline(req, res)
			break
		default:
			log.warn('unkown action ', req.body.action, sess.user)
	}

})

function approve(req, res) {
	let id = req.params.id
	let sess = req.session

	PageChangeRequest
		.findOne({ _id: id })
		.then(request => {

			Page
				.findOne({ _id: request.pageId })
				.then(page => {

					log.info(sess.user.username, 'approved', id)

					change.doDBChange(page, request)
					page.save()
					cachegoose.clearCache('page-' + page.id)

					res.redirect('/page/' + page.id + '/' + page.encodedTitle)

				})
				.catch(err => log.error(res, 'Page findOne approve', id, err))
		})
		.catch(err => log.error(res, 'PageChangeRequest findOne approve', id, err))
}

function decline(req, res) {
	let id = req.params.id
	let sess = req.session

	PageChangeRequest
		.findOne({ _id: id })
		.then(request => {

			res.redirect('/page/' + request.pageId) // let the link fix it self

			if (request.user === 'anonymous') {
				redis.del(request.ip, function(err, result) {
					if (err) return log.error('Redis del remote address', request.ip, err)
				})
			} else {

				User
					.findOne({ username: request.user })
					.then(user => {
						if (!user) return log.error(res, 'User findOne', userId, err)

						user.pendingRequests -= 1
						user.deniedRequests += 1
						user.save()

						sessionManager.reloadUser(user)


					})
					.catch(err => log.error(res, 'User findOne', userId, err))

			}

			request.remove()
			log.info(sess.user.username, 'declined a request')

		})
		.catch(err => log.error(res, 'PageChangeRequest findOne decline', id, err))
}
