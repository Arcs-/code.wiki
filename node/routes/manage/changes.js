let log = require(getGlobal('getLogger'))('Changes')

router.get('/changes', function(req, res, next) {
	let sess = req.session

	transmit(req, res, {
		title: 'Changes',
		view: ['manage', 'changes'],
		user: sess.user
	})

})

router.get('/changes/update', function(req, res, next) {
	log.read({ limit: 40 }).then(logs => res.json(logs))
})
