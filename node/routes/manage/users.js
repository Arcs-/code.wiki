'use strict'

let log = require(getGlobal('getLogger'))('Users')

let mongoose = require('mongoose')
let regexExtender = require(getUtil('regexExtender'))
let sessionManager = require(getGlobal('getSession'))

let User = mongoose.model('User')

const LIMIT = 10

router.all('*', function(req, res, next) {
	let sess = req.session

	if (!sess.user.administratorClearance) {
		sess.error = 'dude! you don\'t have the necessary clearance'
		res.redirect('/error')
	} else next()

})

router.get('/users/action', function(req, res, next) {
	let sess = req.session

	let action = req.query.action
	let id = req.query.id

	User
		.findOne({ _id: id })
		.exec()
		.then(user => {

			if (action === 'delete') {
				log.info(sess.user.username, 'deleted account', user.username)

				sessionManager.destroy(user)
				user.remove()


			} else if (action === 'promote') {
				log.info(sess.user.username, 'promoted account', user.username)
				user.promote()
				user.save()

				sessionManager.reloadUser(user)

			} else if (action === 'lock' || action === 'unlock') {
				user.lock = !user.lock
				log.info(sess.user.username, (user.locked) ? 'locked' : 'unlocked', user.username)
				user.save()

				sessionManager.destroy(user)

			}

			res.redirect('/manage/users')

		})
		.catch(err => log.error(res, 'User findOne ' + id, err))

})

router.get('/users/:page?', function(req, res, next) {
	let sess = req.session
	let pageNumber = (req.params.page || 1) - 1
	let username = req.query.username

	if (username) var query = { username: new RegExp(regexExtender.escape(username), 'i') }
	else var query = {}

	User
		.find(query)
		.sort({ '_id': 'desc' })
		.skip(LIMIT * pageNumber)
		.limit(LIMIT)
		.exec()
		.then(users => {

			User
				.count()
				.exec(function(err, count) {

					transmit(req, res, {
						title: 'Users',
						view: ['manage', 'users'],
						user: sess.user,
						users,
						pageNumber,
						count: LIMIT % count
					})

				})

		})
		.catch(err => log.error(res, 'User find', query, err))

})

module.exports = router
