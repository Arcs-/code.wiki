'use strict'

let log = require(getGlobal('getLogger'))('sitemap')

let childProcess = require('child_process')

let env = Object.create(process.env)
env.NODE_ENV = process.env.NODE_ENV

router.all('*', function(req, res, next) {
	let sess = req.session

	if (!sess.user.administratorClearance) {
		sess.error = 'dude! you don\'t have the necessary clearance'
		res.redirect('/error')
	} else next()

})

router.get('/sitemap', function(req, res, next) {
	let sess = req.session

	transmit(req, res, {
		title: 'Sitemap',
		view: ['manage', 'sitemap'],
		user: sess.user
	})


})

router.post('/sitemap', function(req, res, next) {

	createSitemap(function(err) {
		if (err) return log.error(res, err)
		log.info('created sitemap')

		res.setHeader('Content-disposition', 'attachment; filename=sitemap.xml')
		res.setHeader('Content-type', 'application/xml ')
		res.sendFile('public/sitemap.xml', { root: __rootDir })

	})

})

function createSitemap(callback) {
	let invoked = false

	let process = childProcess.fork(__rootDir + '/sitemap-creator.js', { env })

	process.on('error', function(err) {
		if (invoked) return
		invoked = true
		callback(err)
	})

	process.on('exit', function(code) {
		if (invoked) return
		invoked = true
		let err = code === 0 ? null : new Error('exit code ' + code)
		callback(err)
	})

}
