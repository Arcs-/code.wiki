'use strict'

let log = require(getGlobal('getLogger'))('API')

const SUGGESTION_COUNT = 7

let router = require('express').Router()
let mongoose = require('mongoose')

let Page = mongoose.model('Page')

/* TODO
use json/html switch
http://stackoverflow.com/a/12984730/3137109
*/


router.get('/search/', function(req, res, next) {
	let query = req.query.q || ''

	let searchQuery = {
		query: {
			bool: {
				should: [{
					match: {
						title: {
							query: query.toLowerCase(),
							fuzziness: 'auto',
						}
					}
				}, {
					terms: {
						tags: query.split(' ')
					}
				}]
			}
		},
		size: SUGGESTION_COUNT
	}

	Page
		.esSearch(searchQuery)
		.then(results => {
			res.send(JSON.stringify(results.hits.hits))

		})
		.catch(err => log.error(res, 'Page search', searchQuery, err))

})

module.exports = router
