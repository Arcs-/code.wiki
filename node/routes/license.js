'use strict'

let router = require('express').Router()

router.get('/', function(req, res, next) {
	let sess = req.session

	transmit(req, res, {
		title: 'License',
		view: ['main', 'license'],

		user: sess.user
	})

})

module.exports = router
