let log = require(getGlobal('getLogger'))('Signin')

let mongoose = require('mongoose')
let brute = require(getGlobal('getBrute'))
let rules = require(getGlobal('getRules'))
let sessionManager = require(getGlobal('getSession'))
let url = require('url')

let User = mongoose.model('User')

router.all('/signin', function(req, res, next) {
	if (req.session && req.session.user) return res.redirect('/')
	next()

})

router.get('/signin', function(req, res, next) {

	if (req.headers.referer && (req.session && !req.session.redirect_to)) {
		let currentURL = url.parse(req.headers.referer)
		if (currentURL.hostname === 'wiki.stillh.art' || currentURL.hostname === '127.0.0.1') {
			req.session.redirect_to = currentURL.path
		}
	}

	transmit(req, res, {
		title: 'Sign in',
		view: ['user', 'signin'],

		error: req.session.errorMessage,
		success: req.session.successMessage
	})
	delete req.session.errorMessage
	delete req.session.successMessage

})

router.post('/signin', brute.globalBruteforce, brute.userBruteforce, function(req, res, next) {
	let username = req.body.username.trim()
	let password = req.body.password.trim()
	let redirect_to = req.session.redirect_to

	if (req.tooManyRequest) transmit(req, res, {
		title: 'Sign in',
		view: ['user', 'signin'],

		error: 'Too many login requests (wait ' + req.nextValidRequestDate + ')',
		username,
		password
	})
	else {

		rules.checkLogin(username, password)
			.then(user => {

				req.session.regenerate(function(err) {
					let sess = req.session

					sess.user = User.syncWithSession(user)
					sessionManager.setLink(sess)

					// redeirect to a new page
					if (!redirect_to || redirect_to.startsWith('/user/signin') || redirect_to.startsWith('/user/register')) res.redirect('/')
					else res.redirect(redirect_to)

					delete sess.redirect_to

					log.info(user.username, 'logged in')

				})

			})
			.catch(error => transmit(req, res, {
				title: 'Sign in',
				view: ['user', 'signin'],

				error,
				username,
				password
			}))

	}
})

module.exports = router
