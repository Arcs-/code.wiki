'use strict'

let log = require(getGlobal('getLogger'))('Forgot')

let mongoose = require('mongoose')
let redis = require(getGlobal('getRedis'))('reset')
let session = require('express-session')
let emailValidator = require('email-validator')
let mailer = require(getGlobal('getMailer'))
let crypto = require('crypto')

let User = mongoose.model('User')

router.all('/forgot', function(req, res, next) {
	if (req.session && req.session.user) return res.redirect('/')
	next()

})

router.get('/forgot', function(req, res, next) {
	transmit(req, res, {
		title: 'Reset password',
		view: ['user', 'forgot']
	})
	delete req.session.errorMessage

})

router.post('/forgot', function(req, res, next) {
	let username_email = req.body.username_email

	if (emailValidator.validate(username_email)) var query = { email: username_email }
	else var query = { lowerUsername: username_email.toLowerCase() }

	User
		.findOne(query)
		.select({ email: 1, name: 1, username: 1 })
		.exec()
		.then(user => {

			if (!user) {
				transmit(req, res, {
					title: 'Reset password',
					view: ['user', 'forgot'],
					error: 'Dunno dat account'
				})
			} else {

				redis.incr('restore-' + req.connection.remoteAddress, function(err, result) {
					if (result >= 3) {
						transmit(req, res, {
							title: 'Reset password',
							view: ['user', 'forgot'],
							error: 'you\'ve requested to many recover mails, please wait'
						})
					} else {

						crypto.randomBytes(32, function(err, buffer) {
							let resetKey = buffer.toString('hex')

							redis.set(resetKey, user.username, function(err, result) {
								if (err) return log.error('Redis set reset key', query, err)

								redis.expire(resetKey, 60 * 60 * 24) // remove after a day

							})

							let mail = {
								to: user.email,
								subject: 'Reset your wiki.stillh.art password',
								template: 'reset_password',
								context: {
									name: user.name || user.username,
									link: 'https://wiki.stillh.art/user/reset?key=' + resetKey
								}
							}

							mailer.send(mail)
								.then(info => {
									log.info('sending a recover mail to', user.username)

									transmit(req, res, {
										title: 'Reset password',
										view: ['user', 'forgot'],
										success: 'a mail has been sent'
									})

								})
								.catch(err => {
									log.error(res, 'Send mail', mailOptions, err)

									transmit(req, res, {
										title: 'Reset password',
										view: ['user', 'forgot'],
										error: 'we had an error, please try again later'
									})
								})
						})
					}
				})

			}

		})
		.catch(err => log.error(res, 'findOne', query, err))

})

module.exports = router
