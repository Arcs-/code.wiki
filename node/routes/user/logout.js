'use strict'

let log = require(getGlobal('getLogger'))('Logout')

let url = require('url')

router.get('/logout', function(req, res, next) {
	let sess = req.session

	if(sess.user) log.info(sess.user.username, 'logged out')
	req.session.destroy()

	if (req.headers.referer) {
		let currentURL = url.parse(req.headers.referer)
			// TODO: remove localhost
		if (currentURL.hostname === 'wiki.stillh.art' || currentURL.hostname === 'localhost') return res.redirect(currentURL.path)

	}

	res.redirect('/')

})

module.exports = router
