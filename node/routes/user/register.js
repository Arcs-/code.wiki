'use strict'

let log = require(getGlobal('getLogger'))('Register')

let mongoose = require('mongoose')
let rules = require(getGlobal('getRules'))
let sessionManager = require(getGlobal('getSession'))
let scrypt = require('scrypt')
let configHandler = require(getGlobal('getConfig'))
let configScrypt = configHandler.scrypt

let User = mongoose.model('User')

router.all('/register', function(req, res, next) {
	if (req.session && req.session.user) return res.redirect('/')
	next()

})

router.get('/register', function(req, res, next) {
	transmit(req, res, {
		title: 'Register',
		view: ['user', 'register']
	})
})

router.post('/register', function(req, res, next) {
	let proposedUser = {
		username: req.body.username && req.body.username.trim() || '',
		password: req.body.password && req.body.password.trim() || '',
		passwordRepeat: req.body.passwordRepeat && req.body.passwordRepeat.trim() || '',
		email: req.body.email && req.body.email.trim() || ''
	}

	new Promise((resolve, reject) => {
			if (proposedUser.username == '' || proposedUser.password == '' || proposedUser.passwordRepeat == '' || proposedUser.email == '') return reject('Please fill out all fields')
			else return resolve()
		})
		.then(() => rules.validateUsername(proposedUser))
		.then(() => rules.validateEMail(proposedUser))
		.then(() => rules.validatePassword(proposedUser))
		.then(() => scrypt.kdf(proposedUser.password, configScrypt))
		.then(hashedPassword => {

			let newUser = {
				username: proposedUser.username,
				email: proposedUser.email,
				password: hashedPassword
			}

			User
				.create(newUser)
				.then(insertedUser => {

					req.session.regenerate(function(err) {
						let sess = req.session

						sess.user = User.syncWithSession(insertedUser)
						sessionManager.setLink(sess)

						let redirect_to = sess.redirect_to

						// redeirect to a new page
						if (!redirect_to || redirect_to.startsWith('/user/signin') || redirect_to.startsWith('/user/register')) res.redirect('/')
						else res.redirect(redirect_to)

						delete sess.redirect_to

						log.info('new user', sess.user.username /*, sess.user.email*/ )

					})

				})
				.catch(err => log.error(res, 'User create', newUser, err))

		})
		.catch(function(msg) {

			transmit(req, res, {
				title: 'Register',
				view: ['user', 'register'],
				error: msg,
				proposedUser: proposedUser
			})

		})

})

module.exports = router
