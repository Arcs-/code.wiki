'use strict'

const genericError = 'invalid or expired key'

let log = require(getGlobal('getLogger'))('Reset')

let mongoose = require('mongoose')
let scrypt = require('scrypt')
let redis = require(getGlobal('getRedis'))('reset')
let rules = require(getGlobal('getRules'))
let configHandler = require(getGlobal('getConfig'))
let configScrypt = configHandler.scrypt

let User = mongoose.model('User')

router.all('/reset', function(req, res, next) {
	if (req.session && req.session.user) return res.redirect('/')
	next()

})

router.get('/reset', function(req, res, next) {
	let resetKey = req.query.key

	redis.get(resetKey, function(err, result) {
		if (err) return log.error('Redis get', resetKey, err)

		if (!result) transmit(req, res, {
			title: 'Password Reset',
			view: ['user', 'reset'],
			error: genericError
		})
		else transmit(req, res, {
			title: 'Password Reset',
			view: ['user', 'reset'],
		})

	})

})

router.post('/reset', function(req, res, next) {
	let sess = req.session
	let resetKey = req.query.key

	redis.get(resetKey, function(err, result) {
		if (err) return log.error('Redis get', resetKey, err)
		if (!result) return transmit(req, res, {
			title: 'Password Reset',
			view: ['user', 'reset'],
			error: 'Your key is not valid'
		})

		let proposedPassword = {
			password: req.body.password.trim(),
			passwordRepeat: req.body.passwordRepeat.trim()
		}

		// validate password
		rules.validatePassword(proposedPassword)
			.then(message => User.findOne({ username: result }))
			.then(user => {
				if (!user) {
					log.error('mongoose findOne', result, err)
					transmit(req, res, {
						title: 'Password Reset',
						view: ['user', 'reset'],
						error: 'Sorry, we had an internal error'
					})
					return
				}

				scrypt.kdf(proposedPassword.password, configScrypt)
					.then(hashedPassword => {

						user.password = hashedPassword
						user.save(err => err && log.error('mongoose save', err))

						log.info('changed password for', user.username)

						// deleted entrie
						redis.del(resetKey, (err, result) => {
							if (err) return log.error(res, 'Redis del restore key', request.ip, err)

							// render sign in
							sess.successMessage = 'Your password has been changed'
							res.redirect('/user/signin')
						})

					})
					.catch(err => transmit(req, res, {
						title: 'Password Reset',
						view: ['user', 'reset'],
						error: 'Sorry, we had an internal error'
					}))
			})

			.catch(err => transmit(req, res, {
				title: 'Password Reset',
				view: ['user', 'reset'],
				error: err
			}))

	})

})

module.exports = router
