let log = require(getGlobal('getLogger'))('Settings')

let mongoose = require('mongoose')
let rules = require(getGlobal('getRules'))
let escape = require('escape-html')
let marked = require('marked')
let _ = require('underscore')
let gravatar = require('gravatar')
let hljs = require('highlight.js')

let Page = mongoose.model('Page')
let User = mongoose.model('User')

marked.setOptions({
	sanitize: true,
	singleLineBreaks: true,
	highlight: (code, lang) => hljs.highlightAuto(code, (lang) ? lang.split() : undefined).value
})

let renderer = new marked.Renderer()
renderer.heading = (text, level) => text


router.all('/settings', function(req, res, next) {
	let sess = req.session

	if (sess && !sess.user) {
		sess.redirect_to = '/user/settings'
		sess.errorMessage = 'Please sign in'
		res.redirect('/user/signin')

	} else next()

})

router.get('/settings', function(req, res, next) {
	let sess = req.session

	User
		.findOne({ username: sess.user.username })
		.then(user => {

			user = user.toJSON()
			user.largeIcon = gravatar.url(user.email, { s: '180', d: 'https://wiki.stillh.art/images/profile_default.png' })

			transmit(req, res, {
				title: 'Settings',
				view: ['user', 'settings'],

				user: sess.user,
				realUser: user,
				accountError: sess.accountError
			})
			delete sess.accountError

		})
		.catch(err => log.error(res, 'User findOne', username, err))

})

router.post('/settings', function(req, res, next) {
	let sess = req.session

	switch (req.body.action) {
		case 'profile':

			User
				.findOne({ username: sess.user.username })
				.then(user => {

					let doUpdate = false

					if (req.body.name) {
						let newName = escape(req.body.name.trim())
						if (!_.isEqual(user.name, newName)) {
							user.name = newName
							doUpdate = true
						}
					}

					if (req.body.location) {
						let newLocation = escape(req.body.location.trim())
						if (!_.isEqual(user.location, newLocation)) {
							user.location = newLocation
							doUpdate = true
						}
					}

					if (req.body.website) {
						let newWebsite = escape(req.body.website.trim())
						if (newWebsite.substring(0, 4) != 'http') newWebsite = 'http://' + newWebsite
						if (!_.isEqual(user.website, newWebsite)) {
							user.website = newWebsite
							doUpdate = true
						}
					}

					let newBio = req.body.bio
					if (newBio && !_.isEqual(user.bioRaw, newBio)) {
						user.bioRaw = newBio
						user.bio = marked(newBio, { renderer })
						doUpdate = true
					}

					if (doUpdate) {
						user.save()

						transmit(req, res, {
							title: 'Settings',
							view: ['user', 'settings'],

							user: sess.user,
							realUser: user,
							profileSuccess: 'changes saved!'
						})

					} else {
						transmit(req, res, {
							title: 'Settings',
							view: ['user', 'settings'],

							user: sess.user,
							realUser: user
						})
					}

				})
				.catch(err => {
					log.error(res, 'User findOne', username, err)
					transmit(req, res, {
						title: 'Settings',
						view: ['user', 'settings'],

						user: sess.user,
						realUser: user,
						profileError: 'internal error. we\'re on it'
					})
				})

			break
		case 'email':
			let email = escape(req.body.email)
			let userUpdate = new Promise(function(resolve, reject) {
					if (req.body.password == '' || email == '') return reject('Please fill out password and your new mail')
					else return resolve()
				})
				.then(() => rules.validateEMail({ email })) // TODO: only when update
				.then(() => User.findOne({ username: sess.user.username }))
				.then(user => rules.checkLoginOnLive(user, req.body.currentPassword))
				.then(user => {

					user.email = email
					user.showEMail = !!req.body.showemail
					user.icon = gravatar.url(email, { s: '30', d: 'https://wiki.stillh.art/images/profile_default.png' })
					user.save()

					sess.user = User.syncWithSession(user)

					for (let action of user.actions) {
						if (action.type != 'COMMENT') continue

						Page
							.findOne({ _id: action.pageId })
							.select({ comments: 1 })
							.then(page => {
								page.comments.id(action.changeId).icon = user.icon
								page.save()
							})

					}


					sess.icon = user.gravatar

					transmit(req, res, {
						title: 'Settings',
						view: ['user', 'settings'],

						user: sess.user,
						realUser: user,
						accountSuccess: 'changes saved!'
					})

					return user
				})
				.catch(err => {
					sess.accountError = err
					res.redirect('/user/settings')
				})
			break
		case 'password':

			let proposedChanges = {
				currentPassword: req.body.currentPassword && req.body.currentPassword.trim() || '',
				password: req.body.password && req.body.password.trim() || '',
				passwordRepeat: req.body.passwordRepeat && req.body.passwordRepeat.trim() || ''
			}

			new Promise(function(resolve, reject) {
					if (proposedChanges.currentPassword == '' || proposedChanges.password == '' || proposedChanges.passwordRepeat == '') return reject('Please fill out all fields')
					else return resolve()
				})
				.then(() => rules.validatePassword(proposedChanges))
				.then(() => rules.checkLogin(sess.user.username, proposedChanges.currentPassword))
				.then(user => {

					user.password = proposedChanges.password
					user.save()

					transmit(req, res, {
						title: 'Settings',
						view: ['user', 'settings'],

						user: sess.user,
						realUser: user,
						accountSuccess: 'changes saved!'
					})

				}).catch(err => {
					sess.accountError = err
					res.redirect('/user/settings')
				})

			break
		default:
			log.warn('unkown action ', req.body.action)
	}

})

module.exports = router
