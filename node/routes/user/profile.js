'use strict'

let log = require(getGlobal('getLogger'))('Profile')

let mongoose = require('mongoose')
let gravatar = require('gravatar')

let User = mongoose.model('User')

router.get('/:username?', function(req, res, next) {
	let sess = req.session
	let username = req.params.username

	if (!username || username.length < 1) {
		if (sess && sess.user) return res.redirect('/user/' + sess.user.username)
		else return next()
	}

	User
		.findOne({ lowerUsername: username.toLowerCase() })
		.select({
			username: 1,
			name: 1,
			creation: 1,
			email: 1,
			actions: 1,
			_id: 0,
			location: 1,
			website: 1,
			acceptedRequests: 1,
			bio: 1,
			timeline: 1

		})
		.then(user => {

			if (!user) return next()

			user.actions = user.actions.slice(0, 7)

			let sess = req.session
			transmit(req, res, {
				title: user.username,
				view: ['user', 'profile'],
				foreignUser: user,
				largeIcon: gravatar.url(user.email, { s: '180', d: 'https://wiki.stillh.art/images/profile_default.png' }),
				user: sess.user,
				currentMonth: new Date().getMonth()
			})

		})
		.catch(err => log.error(res, 'User findOne', username, err))

})

module.exports = router
