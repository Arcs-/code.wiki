'use strict'

require('node-import')

let router = require('express').Router()

imports.module('routes/user/forgot', { router })
imports.module('routes/user/logout', { router })
imports.module('routes/user/signin', { router })
imports.module('routes/user/register', { router })
imports.module('routes/user/reset', { router })
imports.module('routes/user/settings', { router })

/*
  page
===================
*/
imports.module('routes/user/profile', { router })

module.exports = router
