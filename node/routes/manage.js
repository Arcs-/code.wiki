'use strict'

require('node-import')

let router = require('express').Router()

router.all('*', function(req, res, next) {
	let sess = req.session

	if (sess && !sess.user) { // not logged in, forward to login
		sess.redirect_to = '/manage' + req.url
		sess.errorMessage = 'Please sign in'
		res.redirect('/user/signin')

	} else if (!sess.user.moderatorClearance) {
		sess.error = 'dude! you don\'t have the necessary clearance'
		res.redirect('/error')

	} else next()

})

// moderatorClearance

imports.module('routes/manage/review', { router })

// administratorClearance

imports.module('routes/manage/users', { router })
imports.module('routes/manage/sitemap', { router })
imports.module('routes/manage/changes', { router })

/*
  page
===================
*/
router.get('/', function(req, res, next) {


})

module.exports = router
