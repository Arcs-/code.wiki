let log = require(getGlobal('getLogger'))('Page')

require('node-import')

let router = require('express').Router()
let mongoose = require('mongoose')
let shortid = require('shortid')
let _ = require('underscore')

let Page = mongoose.model('Page')

imports.module('routes/page/new', { router })

router.all('/:id?/:encodedName?', function(req, res, next) {
	if (!shortid.isValid(req.params.id)) return next('route')
	next()

})

imports.module('routes/page/discuss', { router })
imports.module('routes/page/edit', { router })
imports.module('routes/page/history', { router })

/*
  page
===================
*/
router.get('/:id?/:encodedName?', function(req, res, next) {
	let id = req.params.id

	Page
		.findOne({ _id: id })
		.select({
			lastUpdate: 1,
			title: 1,
			description: 1,
			encodedTitle: 1,
			tags: 1,
			contentRendered: 1,
			links: 1
		})
		.cache('page-' + id)
		.exec()
		.then((page) => {

			if (!page) return next()

			if (!_.isEqual(page.encodedTitle, req.params.encodedName) && page.encodedTitle) return res.redirect('/page/' + id + '/' + page.encodedTitle)

			transmit(req, res, {
				title: page.tags[0] + ' ' + page.title,
				view: ['main', 'page'],

				user: req.session.user,
				committed: req.session.committed,
				page
			})

			if (req.session.committed) req.session.committed = undefined

		})
		.catch((err) => log.error(res, 'Page findOne', id, err))

})

module.exports = router
