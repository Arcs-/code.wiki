'use strict'

let log = require(getGlobal('getLogger'))('Discuss')

let mongoose = require('mongoose')
let _ = require('underscore')
let marked = require('marked')
let hljs = require('highlight.js')
let escape = require('escape-html')
let cachegoose = require('cachegoose')
let time = require(getUtil('time'))

let Page = mongoose.model('Page')
let Comment = mongoose.model('Comment')
let User = mongoose.model('User')

const loadSize = 10

marked.setOptions({
	sanitize: true,
	singleLineBreaks: true,
	highlight: (code, lang) => hljs.highlightAuto(code, (lang) ? lang.split() : undefined).value
})

let renderer = new marked.Renderer()
renderer.heading = (text, level) => text

/*
	Routes
*/

router.get('/:id/discuss', function(req, res, next) {
	let id = req.params.id

	let offset = (req.query.offset && !isNan(req.query.offset)) ? req.query.offset * 1 : 0


	Page
		.findOne({ _id: id })
		.select('title encodedTitle comments')
		//		.slice('comments', [offset, loadSize])
		//.cache('comments-' + id)
		.then(page => {

			if (!page) return next()

			let sess = req.session
			transmit(req, res, {
				title: `Discuss - ${page.title}`,
				view: ['page', 'discuss'],

				user: sess.user,
				page: page
			})

		})
		.catch(err => log.error(res, 'Page findOne', id, err))

})

router.post('/:id/discuss', function(req, res, next) {
	let sess = req.session

	if (!sess.user) { // not logged in, forward to login
		sess.redirect_to = '/page/' + id + '/discuss'
		sess.errorMessage = 'Please sign in to discuss'
		res.redirect('/user/signin')
		return
	}

	if ('action' in req.body && req.body.action === 'flag') flag(req, res)
	else if ('action' in req.body && req.body.action === 'post') newComment(req, res)
	else log.warn('unkown action', req.body && req.body.action)

})

function newComment(req, res) {
	let id = req.params.id
	let sess = req.session

	let pagePromise = Page
		.findOne({ _id: id })
		.then(page => {

			if (!page) return next()

			let newComment = {
				content: marked(req.body.content, { renderer }),
				username: sess.user.username,
				icon: sess.user.icon
			}

			if ('title' in req.body) newComment.title = escape(req.body.title).trim()

			log.info(sess.user.username, 'posted a comment on', page.encodedTitle)

			if ('parent' in req.body) var comments = page.comments.id(req.body.parent).replies
			else var comments = page.comments

			let postedComment = new Comment(newComment)
			comments.unshift(postedComment)

			page.save()

			page.last = postedComment

			return page
		})

	let userPromise = User.findOne({ username: sess.user.username })

	Promise.all([pagePromise, userPromise])
		.then(results => {
			let page = results[0]
			let user = results[1]

			res.json(page)

			user.actions.unshift({
				pageId: id,
				changeId: page.last._id,
				comment: page.last.title || 'reply',
				type: 'COMMENT'
			})

			user.timeline[new Date().getMonth()] = (user.timeline[new Date().getMonth()] || 0) + 1
			user.markModified('timeline')

			user.save()

		})
		.catch(err => log.error(res, 'promise all', err))

}

function flag(req, res) {
	let sess = req.session
	let id = req.body.flag

	log.info(sess.user.username, 'flaged an answer on', id)

	res.json({ status: 'ok' })

}
