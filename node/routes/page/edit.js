'use strict'

let log = require(getGlobal('getLogger'))('Edit')

let mongoose = require('mongoose')
let redis = require(getGlobal('getRedis'))('brute')
let _ = require('underscore')

let escape = require('escape-html')
let cachegoose = require('cachegoose')
let url = require('url')
let time = require(getUtil('time'))
let change = require(getUtil('change'))

let Page = mongoose.model('Page')
let PageChangeRequest = mongoose.model('PageChangeRequest')
let User = mongoose.model('User')

router.all('/:id/edit', function(req, res, next) {
	let sess = req.session

	if (sess.user) {

		if (User.shouldBlock(sess.user)) req.limitReached = true
		else req.limitReached = false

		next()

	} else {

		redis.get(req.connection.remoteAddress, function(err, result) {
			if ((result || 0) >= 1) req.limitReached = 'You need to be logged in to have more requests'
			else req.limitReached = false

			next()

		})

	}

	/* no login to edit
	if (sess && !sess.user) { // not logged in, forward to login
		sess.redirect_to = '/page/' + req.params.id + '/edit'
		sess.errorMessage = 'Please sign in to edit'
		res.redirect('/signin')

	} else next()
	*/

})

router.get('/:id/edit', function(req, res, next) {
	let id = req.params.id
	let sess = req.session

	Page
		.findOne({ _id: id })
		.then(page => {
			if (!page) return next()

			transmit(req, res, {
				title: `Edit - ${page.title}`,
				view: ['page', 'edit'],

				user: sess.user,
				limitReached: req.limitReached,
				page
			})

		})
		.catch(err => log.error(res, 'Page findOne', id, err))

})

router.post('/:id/edit', function(req, res, next) {
	let id = req.params.id
	let sess = req.session

	if (req.limitReached) return res.redirect('/page/' + id + '/edit')

	if(req.body.action === 'delete') deleteRequest(req, res)
	else saveRequest(req, res) // 'save' or just enter press

})

function deleteRequest(req, res) {
	let id = req.params.id
	let sess = req.session

	if (sess.user && sess.user.moderatorClearance) {
		Page
			.findOne({ _id: id })
			.then(page => {

				if (!page) return next()

				page.remove()
				log.info(sess.user.username, 'deleted (as moderator)', currentPage.encodedTitle)

			})
			.catch(err => log.error(res, 'Page findOne delete', id, err))

		res.redirect('/')
	}
}

function saveRequest(req, res) {
	let id = req.params.id
	let sess = req.session

	let title = escape(req.body.title).trim()
	if (_.isEqual(title, 'discuss')) title += 1
	else if (_.isEqual(title, 'edit')) title += 1
	else if (_.isEqual(title, 'history')) title += 1

	let content = req.body.content.trim()

	// tags
	let tags = req.body.tags.split(',') || []
	if (req.body.transientTagField) tags.push(req.body.transientTagField)
	for (let i = 0; i < tags.length; i++) {
		if (!/^[a-zA-Z0-9\.\-+]{2,20}$/g.test(tags[i])) {
			log.warn('Tag mismatch create', i, tags[i])
			tags.splice(i, 1)
		}
	}


	// links
	if (!req.body.linkTitle) req.body.linkLink = req.body.linkTitle = []
	let links = []
	let addLink = function(link, title) {
		if(link.length === 0 || title.link === 0) return
		if (link.substring(0, 4) != 'http') link = 'http://' + link
		links.push({
			title: escape(title),
			link: escape(link),
			displayLink: url.parse(link).hostname
		})
	}

	if(typeof req.body.linkLink == 'string') addLink(req.body.linkLink, req.body.linkTitle)
  else for (let i = 0; i < req.body.linkTitle.length; i++) addLink(req.body.linkLink[i], req.body.linkTitle[i])

	/*
			load from db
			*/
	Page
		.findOne({ _id: id })
		.then(currentPage => {

			if (!currentPage) return next()

			let titleEqual = _.isEqual(currentPage.title, title)
			let tagsEqual = _.isEqual(currentPage.tags, tags)
			let linksEqual = _.isEqual(currentPage.links, links)
			let contentEqual = _.isEqual(currentPage.content, content)

			// check if a chance happend at all
			if (titleEqual && tagsEqual && linksEqual && contentEqual) {
				res.redirect('/page/' + id + '/' + currentPage.encodedTitle)
				return
			}

			let changeRequest = {
				pageId: currentPage._id,
				version: currentPage.version + 1,
				changeTitle: escape(req.body.changeTitle),
				date: time.getNiceDate(),
				title,
				tags,
				links,
				content
			}

			if (sess.user) changeRequest.user = sess.user.username
			else {
				changeRequest.user = 'anonymous'
				changeRequest.ip = req.connection.remoteAddress
			}

			//* emmediat update
			if (sess.user && sess.user.moderatorClearance) {

				log.info(sess.user.username, 'edited (as moderator)', currentPage.encodedTitle, changeRequest.version)

				change.doDBChange(currentPage, changeRequest)

				currentPage.save()
				cachegoose.clearCache('page-' + id)

				req.session.committed = true
				res.redirect('/page/' + id + '/' + currentPage.encodedTitle)

			} else {

				PageChangeRequest
					.create(changeRequest)
					.then(posted => {

						req.session.committed = true
						res.redirect('/page/' + currentPage._id + '/' + currentPage.encodedTitle)

						if (sess.user) {

							log.info(changeRequest.user, 'proposed edit', currentPage.encodedTitle, sess.user.pendingRequests)

							sess.user.pendingRequests += 1
							sess.save()

							User.syncWithDB(sess.user, ['pendingRequests'])

						} else {

							log.info(changeRequest.ip, 'proposed edit', currentPage.encodedTitle, changeRequest.version)

							redis.incr(changeRequest.ip, function(err, result) {
								if (err) return log.error('Redis incr remote adress create', changeRequest.ip, err)

								redis.expire(changeRequest.ip, 60 * 60 * 24) // reopen after a day in any case

							})

						}

					})
					.catch(err => log.error(res, 'PageChangeRequest create', changeRequest, err))

			}

		})
		.catch(err => log.error(res, 'Page findOne', id, err))
}
