'use strict'

let log = require(getGlobal('getLogger'))('New')

require('node-import')

let mongoose = require('mongoose')
let marked = require('marked')
let change = require(getUtil('change'))
let hljs = require('highlight.js')
let escape = require('escape-html')
let url = require('url')
let time = require(getUtil('time'))
let regexExtender = require(getUtil('regexExtender'))

let Page = mongoose.model('Page')
let User = mongoose.model('User')

router.all('/new', function(req, res, next) {
	let sess = req.session
	if (sess && !sess.user) { // not logged in, forward to login
		sess.redirect_to = '/page/new'
		sess.errorMessage = 'Please sign in to create new content'
		res.redirect('/user/signin')

	} else next()

})

router.get('/new', function(req, res, next) {
	let sess = req.session

	transmit(req, res, {
		title: `Share Knowledge`,
		view: ['page', 'new'],

		user: sess.user,
		page: {}
	})

})

router.post('/new', function(req, res, next) {
	let sess = req.session

	let title = escape(req.body.title).trim()
	if (title.length === 0) return // TODO error

	//tags
	let tags = req.body.tags.split(',') || []
	if (req.body.transientTagField) tags.push(req.body.transientTagField)
	for (let i = 0; i < tags.length; i++) {
		if (!/^[a-zA-Z0-9\.\-+]{2,20}$/g.test(tags[i])) {
			log.warn('Tag mismatch create', i, tags[i])
			tags.splice(i, 1)
		}
	}

	if (!req.body.linkTitle) req.body.linkLink = req.body.linkTitle = []
	let links = []
	let addLink = function(link, title) {
		if(link.length === 0 || title.link === 0) return
		if (link.substring(0, 4) != 'http') link = 'http://' + link
		links.push({
			title: escape(title),
			link: escape(link),
			displayLink: url.parse(link).hostname
		})
	}

	if(typeof req.body.linkLink == 'string') addLink(req.body.linkLink, req.body.linkTitle)
  else for (let i = 0; i < req.body.linkTitle.length; i++) addLink(req.body.linkLink[i], req.body.linkTitle[i])


	let newPage = {
		version: 0,
		lastUpdate: time.getNiceDate(),
		title,
		encodedTitle: regexExtender.urlCleaner(title),
		lowerTitle: title.toLowerCase(),
		tags,
		links,
		content: req.body.content,
		contentRendered: marked(req.body.content, change.markedConf),
		history: []
	}

	newPage.description = change.descriptionator(newPage)

	newPage.history.push({
		version: newPage.version,
		date: newPage.lastUpdate,
		user: sess.user.username,
		changeTitle: 'Initial',

		title: newPage.title,
		tags: newPage.tags,
		content: newPage.content,
		links: newPage.link
	})



	Page
		.create(newPage)
		.then((posted, err) => {
			if (err) return log.error(res, 'page new', newPage, err)

			/*
			log action
			*/
			log.info(sess.user.username, 'created ', posted.encodedTitle)

			User
				.findOne({ username: sess.user.username })
				.then((user, err) => {
					if (err) return log.error(res, 'user findOne new page', sess.user.username, err)

					user.actions.unshift({
						pageId: posted._id,
						comment: posted.title,
						type: 'NEW_PAGE'
					})

					user.timeline[new Date().getMonth()] += 1
					user.markModified('timeline')

					user.save()

				})

			res.redirect('/page/' + posted._id + '/' + posted.encodedTitle)

		})

})

module.exports = router
