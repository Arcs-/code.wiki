'use strict'

let log = require(getGlobal('getLogger'))('History')

let mongoose = require('mongoose')
let _ = require('underscore')

let JsDiff = require('diff')
let change = require(getUtil('change'))
let escape = require('escape-html')
let marked = require('marked')
let hljs = require('highlight.js')

let Page = mongoose.model('Page')

marked.setOptions({
	sanitize: true,
	singleLineBreaks: true,
	highlight: (code, lang) => hljs.highlightAuto(code, (lang) ? lang.split() : undefined).value
})

let renderer = new marked.Renderer()
renderer.heading = function(text, level) {
	let escapedText = text.toLowerCase().replace(/[^\w]+/g, '-')
	return '<h2>' + text + '</h2>'
}

router.get('/:id/history/:version?', function(req, res, next) {
	let id = req.params.id
	let version = req.params.version

	Page
		.findOne({ _id: id })
		.then(page => {

			let sess = req.session

			if (!page) return next()

			if (!version ||
				isNaN(version) ||
				version * 1 < 0 ||
				page.history.length <= version * 1 ||
				page.history.length == 0) {

				transmit(req, res, {
					title: `History - ${page.title}`,
					view: ['page', 'history'],

					user: sess.user,
					page: page
				})

			} else {

				version = Math.abs(version - page.history.length + 1)
				let oldVersion = Math.min(version + 1, page.history.length - 1)

				let askedPage = page.history[version]

				if (version != oldVersion) {
					let oldPage = page.history[oldVersion]

					let diffTitle = change.renderDiff(JsDiff.diffWords(oldPage.title, askedPage.title))

					let diffTags = []
					let maxTags = Math.max(oldPage.tags.length, askedPage.tags.length)
					for (let i = 0; i < maxTags; i++) {
						diffTags.push(change.renderDiff(JsDiff.diffWords(oldPage.tags[i] || '', askedPage.tags[i] || '')))
					}

					//	let diffContent = change.renderDiff(JsDiff.diffWords(marked(oldPage.content, { renderer }), marked(askedPage.content, { renderer })))
					let diffContent = change.renderDiff(JsDiff.diffWords(oldPage.content, askedPage.content))
					let lines = diffContent.split('\n')
					lines = change.jumpLines(lines)
					diffContent = lines.join('\n') + '\n'

					let diffLinks = ''
					let max = Math.max(oldPage.links.length, askedPage.links.length)
					for (let i = 0; i < max; i++) {
						if (oldPage.links && oldPage.links[i]) var stringOld = oldPage.links[i].title + ' → ' + oldPage.links[i].link
						else var stringOld = ''

						if (askedPage.links && askedPage.links[i]) var stringnew = askedPage.links[i].title + ' → ' + askedPage.links[i].link
						else var stringnew = ''

						diffLinks += change.renderDiff(JsDiff.diffWords(stringOld, stringnew)) + '\n'
					}

					transmit(req, res, {
						title: `History - ${page.title}`,
						view: ['page', 'historyDetail'],

						user: sess.user,
						page: page,
						change: askedPage,
						diff: {
							oldVersion: oldPage.version,
							diffTitle,
							diffTags,
							diffContent,
							diffLinks
						}
					})
				} else { // first version (no older version)

					transmit(req, res, {
						title: `History - ${page.title}`,
						view: ['page', 'historyDetail'],

						user: sess.user,
						page: page,
						change: askedPage,
						diff: {
							oldVersion: '',
							diffTitle: escape(askedPage.title),
							diffTags: escape(askedPage.tags),
							diffContent: escape(askedPage.content),
							diffLinks: escape(askedPage.links)
						}
					})
				}
			}

		})
		.catch(err => log.error(res, 'Page findOne', id, err))

})
