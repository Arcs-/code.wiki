'use strict'

let log = require(getGlobal('getLogger'))('Search')

const PAGE_SIZE = 10

let router = require('express').Router()
let mongoose = require('mongoose')

let Page = mongoose.model('Page')

router.get('/', function(req, res, next) {

	let page = 0
	if (req.query.page && !isNaN(req.query.page)) page = Math.max(req.query.page - 1, 0)

	if(req.query.q) {
		if(typeof req.query.q === 'string') var query = req.query.q
		else var query = req.query.q[0] || ''
	} else var query = ''

	let searchQuery = {
		query: {
			bool: {
				should: [{
					match: {
						title: {
							query: query.toLowerCase(),
							fuzziness: 'auto',
							zero_terms_query: 'all'
						}
					}
				}, {
					terms: {
						tags: query.split(' ')
					}
				}]
			}
		},
		sort: [
			{ lastUpdate: { order: "desc" } }
		],
		from: page * PAGE_SIZE,
		size: PAGE_SIZE
	}

	if (req.query.tags) {
		var tags = (req.query.tags + (req.query.t || '')).split(',')
		searchQuery.query.bool.must = []
		for (let tag of tags)
			searchQuery.query.bool.must.push({
				'term': {
					'tags': tag.toLowerCase()
				}
			})
	}

	Page
		.esSearch(searchQuery)
		.then(results => {

			let sess = req.session

			transmit(req, res, {
				title: (query || 'Everything') + ((tags) ? ' on ' + tags.join(' and ') : ''),
				view: ['main', 'search'],

				user: sess.user,
				pages: results.hits.hits,
				pageNavigation: createPageNavigation(++page, query, results.hits.total > page * PAGE_SIZE),
				key: query,
				tags
			})

		})
		.catch(err => log.error(res, 'Page search', searchQuery, err))

})

function createPageNavigation(currentPage, query, nextPage) {
	if (query) var baseLink = '?q=' + query + '&page='
	else var baseLink = '?page='

	let navigation = []

	// previous pages
	let tmp = currentPage - 1
	if (tmp > 0) navigation.push('<a class="button small" href="' + baseLink + tmp + '">' + tmp + '</a>')

	// current page
	navigation.push('<a class="button small button-primary">' + currentPage + '</a>')

	// next pages
	tmp = currentPage + 1
	if (nextPage) {
		navigation.push('<a class="button small" href="' + baseLink + tmp + '">' + tmp + '</a> ... ')
		navigation.push('<a class="button" href="' + baseLink + tmp + '">Next</a>')
	}

	// set the 1 page link
	if (currentPage > 2) navigation.unshift('<a class="button small" href="' + baseLink + 1 + '">1</a> ...')

	return navigation.join(' ')

}

module.exports = router
